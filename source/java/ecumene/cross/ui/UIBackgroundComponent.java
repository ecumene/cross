package ecumene.cross.ui;

import javax.vecmath.Vector2f;
import javax.vecmath.Vector4f;

import org.lwjgl.opengl.GL11;

import ecumene.asset.AssetLoader;
import ecumene.cross.ui.geom.Rectangle;
import ecumene.data.Color;
import ecumene.geom.builder.Face;
import ecumene.geom.builder.MeshBuilder;
import ecumene.geom.builder.Vertex;
import ecumene.geom.ogl.OGLGeomBuffer;
import ecumene.opengl.GLPrimitive;
import ecumene.opengl.shader.Pipeline;
import ecumene.opengl.texture.ITexture;

public class UIBackgroundComponent extends UIComponent {
	private static OGLGeomBuffer geomBuffer;
	
	private MeshBuilder model;
	private Color     background;
	private ITexture  texture;
	private Rectangle bounding;
	
	public UIBackgroundComponent(UINode parent, ITexture texture, Color color, Rectangle bounding) {
		super(parent);
		this.background = color;
		this.bounding   = bounding;
	}
	
	public UIBackgroundComponent(UINode parent, ITexture texture, Rectangle bounding) {
		this(parent, texture, new Color(1, 1, 1, 1), bounding);
	}
	
	public UIBackgroundComponent(UINode parent, Color color, Rectangle bounding) {
		this(parent, null, color, bounding);
	}
	
	@Override
	public void init(AssetLoader globalAssets) throws Throwable {
		super.init(globalAssets);
		Rectangle rectangle = bounding;
		if(bounding == null) rectangle = getParent().getBounding();
		
		model = new MeshBuilder(GLPrimitive.TRIANGLES);
		model.add(0, new Vertex(new Vector4f(                      0, rectangle.dimension.y, 0, 1), new Vector2f(0, 1)));
		model.add(1, new Vertex(new Vector4f(                      0,                     0, 0, 1), new Vector2f(0, 0)));
		model.add(2, new Vertex(new Vector4f( rectangle.dimension.x,                      0, 0, 1), new Vector2f(1, 0)));
		model.add(3, new Vertex(new Vector4f( rectangle.dimension.x, rectangle.dimension.y,  0, 1), new Vector2f(1, 1)));
		model.addFace(0, new Face(0, 1, 2, 2, 3, 0));
		
		if(geomBuffer == null) geomBuffer = new OGLGeomBuffer();
	}
	
	@Override
	public void render(Pipeline pipeline, UICamera camera) throws Throwable {
		super.render(pipeline, camera);
		Rectangle rectangle = bounding;
		if(bounding == null) rectangle = getParent().getBounding();

		if(texture != null) texture.bind();
		else                pipeline.setUniformi("u_sampleTexture", GL11.GL_FALSE);
		pipeline.setUniformf("u_cMod", background.r, background.g, background.b, background.a);
		pipeline.setUniformM4("u_tObject", true, rectangle.getTransform());
		geomBuffer.add(model.getOGLMesh(), rectangle.getTransform());
		geomBuffer.upload();
		geomBuffer.render();
		geomBuffer.clear();
	}
}
