package ecumene.cross.ui;

import javax.vecmath.Matrix4f;

import ecumene.cross.CrossLauncher;
import ecumene.cross.world.dynamic.ProjectionTransformComponent;
import ecumene.opengl.shader.Pipeline;

public class UICamera extends UINode {
	protected Matrix4f projection, view;
	
	public UICamera() {
		view = new Matrix4f(); view.setIdentity();
	}
	
	@Override
	public void render(Pipeline pipeline, UICamera camera) throws Throwable {
		super.render(pipeline, camera);
		
		if(CrossLauncher.getInstance().getWindow().resized()){
			projection = ProjectionTransformComponent.createOrthographic(0, CrossLauncher.getInstance().getWindow().getMode().getWidth(), CrossLauncher.getInstance().getWindow().getMode().getHeight(), 0, -10, 10);
			projection.transpose();
		}
		
		pipeline.setUniformM4("u_tProjection", false, projection);
		pipeline.setUniformM4("u_tView", true, view);
	}
}
