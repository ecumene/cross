package ecumene.cross.ui;

import java.util.ArrayList;
import java.util.List;

import javax.vecmath.Matrix4f;

import ecumene.EcuException;
import ecumene.asset.AssetLoader;
import ecumene.cross.CrossLauncher;
import ecumene.cross.ui.geom.Rectangle;
import ecumene.cross.world.WorldGameContext;
import ecumene.opengl.shader.Pipeline;
import ecumene.scene.ISceneComponent;
import ecumene.scene.ISceneGraph;
import ecumene.scene.ISceneNode;

public class UINode implements ISceneNode, IUILoopable {
	public UIContext uiContext;
	public UINode    parent;
	public Matrix4f  transform;
	
	private Rectangle bounding;
	
	public List<ISceneComponent> components;
	public List<ISceneNode>      children;
	
	public UINode(){
		transform = new Matrix4f();
		transform.setIdentity();
		children   = new ArrayList<ISceneNode>();
		components = new ArrayList<ISceneComponent>();
	}
	
	public UIContext getContext(){
		return ((WorldGameContext) CrossLauncher.getInstance().getContext()).getUIContext();
	}
	
	public Matrix4f getTransform(){
		if(parent != null && parent.getTransform() != null){
			Matrix4f out = new Matrix4f(parent.getTransform()); 
			out.setIdentity();
			out.mul(transform);
			return out;
		} else return transform;
	}
	
	public Matrix4f getPrivateTransform(){
		return transform;
	}
	
	public void setBounding(Rectangle bounding){
		this.bounding = bounding;
	}
	
	public Rectangle getBounding(){
		return bounding;
	}

	@Override
	public ISceneGraph getScene() {
		return uiContext.getSceneGraph();
	}

	@Override
	public ISceneNode getParent() {
		return parent;
	}

	@Override
	public void addChild(ISceneNode node) {
		if(!(node instanceof UINode)) 
			throw new EcuException("Given scene node must be a world node");
		((UINode) node).parent = this;
		children.add((UINode) node);
	}

	@Override
	public void addComponent(ISceneComponent node) {
		if(!(node instanceof UIComponent)) 
			throw new EcuException("Given node component must be a world component");
		components.add((UIComponent) node);
	}

	@Override
	public List<ISceneNode> getChildren() {
		return children;
	}

	@Override
	public List<ISceneComponent> getComponents() {
		return components;
	}

	@Override
	public void init(AssetLoader globalAssets) throws Throwable {
		for(int i = 0; i < components.size(); i++){
			((UIComponent) components.get(i)).init(globalAssets);
		}
		
		for(int i = 0; i < children.size(); i++){
			((UINode) children.get(i)).init(globalAssets);
		}
	}

	@Override
	public void render(Pipeline pipeline, UICamera camera) throws Throwable {
		for(int i = 0; i < components.size(); i++){
			((UIComponent) components.get(i)).render(pipeline, camera);
		}
		
		for(int i = 0; i < children.size(); i++){
			((UINode) children.get(i)).render(pipeline, camera);
		}
	}

	@Override
	public void update(float deltaTime) throws Throwable {
		for(int i = 0; i < components.size(); i++){
			((UIComponent) components.get(i)).update(deltaTime);
		}
		
		for(int i = 0; i < children.size(); i++){
			((UINode) children.get(i)).update(deltaTime);
		}
	}

	@Override
	public void destroy() throws Throwable {
		for(int i = 0; i < components.size(); i++){
			((UIComponent) components.get(i)).destroy();
		}
		
		for(int i = 0; i < children.size(); i++){
			((UINode) children.get(i)).destroy();
		}
	}
}
