package ecumene.cross.ui;

import javax.vecmath.Matrix4f;
import javax.vecmath.Vector2f;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;

import org.lwjgl.opengl.GL11;

import ecumene.asset.AssetLoader;
import ecumene.cross.CrossLauncher;
import ecumene.cross.action.IAction;
import ecumene.cross.common.BitmapFont;
import ecumene.cross.ui.geom.Rectangle;
import ecumene.data.Color;
import ecumene.geom.builder.Face;
import ecumene.geom.builder.MeshBuilder;
import ecumene.geom.builder.Vertex;
import ecumene.geom.ogl.OGLGeomBuffer;
import ecumene.glfw.GLFWMouse;
import ecumene.opengl.GLPrimitive;
import ecumene.opengl.shader.Pipeline;
import ecumene.opengl.texture.ITexture;

public class UIButton extends UINode implements IAction {
	private int state;
	private static OGLGeomBuffer geomBuffer;
	
	public ITexture tAsleep, tHovered, tTriggered;
	public Color    cAsleep, cHovered, cTriggered;
	public Color    cTextColor;
	private MeshBuilder model;
	private String text;
	private int charSize;
	
	public UIButton(String text, BitmapFont font, int charSize, Vector3f position) {
		this.text = text;
		this.charSize = charSize * 2;
		
		cAsleep = new Color(0, 0, 0, 1);
		cHovered = new Color(0.4f, 0.4f, 0.4f, 1);
		cTriggered = new Color(0.8f, 0.8f, 0.8f, 1);
		cTextColor = new Color(1, 1, 1, 1);
		
		Rectangle rectangle = new Rectangle(new Vector2f(text.length() * charSize, charSize), position);
		this.setBounding(rectangle);
	}
	
	@Override
	public void init(AssetLoader globalAssets) throws Throwable {
		super.init(globalAssets);		
		model = new MeshBuilder(GLPrimitive.TRIANGLES);
		model.add(0, new Vertex(new Vector4f(                              0, getBounding().dimension.y*2, 0, 1), new Vector2f(0, 1)));
		model.add(1, new Vertex(new Vector4f(                              0,                           0, 0, 1), new Vector2f(0, 0)));
		model.add(2, new Vertex(new Vector4f( getBounding().dimension.x*2, 0,                           0, 1),    new Vector2f(1, 0)));
		model.add(3, new Vertex(new Vector4f( getBounding().dimension.x*2, getBounding().dimension.y*2, 0, 1),    new Vector2f(1, 1)));
		model.addFace(0, new Face(0, 1, 2, 2, 3, 0));
		
		if(geomBuffer == null) geomBuffer = new OGLGeomBuffer();
	}
	
	@Override
	public void render(Pipeline pipeline, UICamera camera) throws Throwable {
		super.render(pipeline, camera);
		
		Color color      = new Color();
		ITexture texture = null;
		
		if(state == 0){
			if(tAsleep != null) texture = tAsleep;
			if(cAsleep != null) color   = cAsleep;
		} else if(state == 1){
			if(tHovered != null) texture = tHovered;
			if(cHovered != null) color   = cHovered;
		} else if(state == 2){
			if(tTriggered != null) texture = tTriggered;
			if(cTriggered != null) color   = cTriggered;
		}
		
		Matrix4f translationModMatrix = new Matrix4f();
		translationModMatrix.setIdentity();
		translationModMatrix.setTranslation(new Vector3f(-getBounding().dimension.x, -getBounding().dimension.y, 0));
		Matrix4f objectMatrix = getBounding().getTransform();
		objectMatrix.mul(translationModMatrix);
		
                            pipeline.setUniformi("u_sampleTexture", GL11.GL_TRUE);
		if(texture != null) texture.bind();
		else                pipeline.setUniformi("u_sampleTexture", GL11.GL_FALSE);
		pipeline.setUniformf("u_cMod", color.r, color.g, color.b, color.a);
		pipeline.setUniformM4("u_tObject", true, objectMatrix);
		geomBuffer.add(model.getOGLMesh(), getBounding().getTransform());
		geomBuffer.upload();
		geomBuffer.render();
		geomBuffer.clear();
		
        pipeline.setUniformi("u_sampleTexture", GL11.GL_TRUE);
		pipeline.setUniformf("u_cMod", cTextColor.r, cTextColor.g, cTextColor.b, cTextColor.a);
		
		Matrix4f matrix = new Matrix4f(objectMatrix);
		Matrix4f invTranslation = new Matrix4f(translationModMatrix);
		invTranslation.invert();
		translationModMatrix.setIdentity();
		translationModMatrix.setTranslation(new Vector3f(-getBounding().dimension.x / 2, -getBounding().dimension.y / 2, 0));
		matrix.mul(translationModMatrix);
		matrix.mul(invTranslation);
		matrix.m03 *= 2f;
		matrix.m13 *= 2f;
		matrix.m23 *= 2f;
		pipeline.setUniformM4("u_tObject", true, matrix);
				
		getContext().getBitmapFont().renderString(text, charSize);
	}
	
	int lastState;
	
	@Override
	public void update(float deltaTime) throws Throwable {
		super.update(deltaTime);
		Rectangle mouse = new Rectangle(new Vector2f(1, 1), 
				new Vector3f((float) CrossLauncher.getInstance().getWindow().getMouse().getX(), (float) CrossLauncher.getInstance().getWindow().getMouse().getY(), getBounding().position.z));
		
		if(getBounding().isColliding(mouse) && !CrossLauncher.getInstance().getWindow().getMouse().isGrabbed()){
			state = 1;
			if(CrossLauncher.getInstance().getWindow().getMouse().isButtonDown(GLFWMouse.MOUSE_BUTTON_1)) state = 2;
			if(CrossLauncher.getInstance().getWindow().getMouse().isButtonDown(GLFWMouse.MOUSE_BUTTON_2)) state = 3;
		} else {
			state = 0; 
		}		
		
		lastState = state;
	}
	
	public boolean isAsleep(){
		return state == 0;
	}
	
	public boolean isHovered(){
		return state == 1;
	}
	
	@Override
	public boolean isTriggered() {
		return state == 2;
	}
}