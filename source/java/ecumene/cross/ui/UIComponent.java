package ecumene.cross.ui;

import ecumene.asset.AssetLoader;
import ecumene.opengl.shader.Pipeline;
import ecumene.scene.ISceneComponent;

public class UIComponent implements ISceneComponent, IUILoopable {
	private UINode parent;
	
	public UIComponent(UINode parent){
		this.parent = parent;
	}
	
	public UINode getParent(){
		return parent;
	}

	@Override public void init(AssetLoader globalAssets)             throws Throwable { }
	@Override public void render(Pipeline pipeline, UICamera camera) throws Throwable { }
    @Override public void update(float deltaTime)                    throws Throwable { }
	@Override public void destroy()                                  throws Throwable { }
}