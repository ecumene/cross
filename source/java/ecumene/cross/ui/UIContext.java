package ecumene.cross.ui;

import java.nio.ByteBuffer;

import javax.vecmath.Vector2f;
import javax.vecmath.Vector3f;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL14;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;

import ecumene.asset.AssetLoader;
import ecumene.cross.CrossLauncher;
import ecumene.cross.common.BitmapFont;
import ecumene.cross.ui.geom.Rectangle;
import ecumene.cross.world.WorldGameContext;
import ecumene.opengl.Frame;
import ecumene.opengl.shader.Pipeline;
import ecumene.opengl.shader.ShaderAsset;
import ecumene.opengl.texture.ITexture;
import ecumene.opengl.texture.Texture;

public class UIContext {
	protected UISceneGraph sceneGraph;
	protected Frame        frame;
	protected Pipeline     defaultPipeline;
	protected UICamera     camera;
	protected Vector2f     frameDimension;
	
	private BitmapFont  font;
	private AssetLoader loader;
	
	public static Rectangle screenBounds;
	
	public UIContext(Vector2f dimensions) {
		camera         = new UICamera();
		frameDimension = dimensions;
		font           = new BitmapFont(null, 16, 24);
		
		sceneGraph = new UISceneGraph(new UINode());
		sceneGraph.root.addChild(camera);
	}
	
	public void init(AssetLoader globalAssets) throws Throwable {
		loader = globalAssets;
		sceneGraph.init(globalAssets);
				
		defaultPipeline = new Pipeline();
		defaultPipeline.attachShader((ShaderAsset) globalAssets.getAsset("pipeline.ui.default-fs"));
		defaultPipeline.attachShader((ShaderAsset) globalAssets.getAsset("pipeline.ui.default-vs"));
		defaultPipeline.link();
	}

	public void render() throws Throwable {
		
		int frameX = frameDimension == null ? CrossLauncher.getInstance().getWindow().getMode().getWidth()  : (int)frameDimension.x;
		int frameY = frameDimension == null ? CrossLauncher.getInstance().getWindow().getMode().getHeight() : (int)frameDimension.y;
		
		font.parent = (ITexture) ((WorldGameContext)CrossLauncher.getInstance().getContext()).getLoader().getAsset("image.font.pixmix");
		
		if(CrossLauncher.getInstance().getWindow().resized()){
			if(frame != null){ frame.destroy(true); }
			
			Texture colorTexture = new Texture(GL11.GL_TEXTURE_2D, 0, GL11.GL_LINEAR);
			colorTexture.bind();
			GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, GL11.GL_RGBA8, (int)frameX, (int)frameY, 0, GL11.GL_RGB, GL11.GL_FLOAT, (ByteBuffer) null);
			
			Texture depthTexture = new Texture(GL11.GL_TEXTURE_2D, 0, GL11.GL_LINEAR);
			depthTexture.bind();
			GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, GL14.GL_DEPTH_COMPONENT32, (int)frameX, (int)frameY, 0, GL11.GL_DEPTH_COMPONENT, GL11.GL_FLOAT, (ByteBuffer) null);
			
			frame = new Frame(GL30.GL_FRAMEBUFFER); 
			frame.bind();
			frame.bufferTexture(0, colorTexture, GL30.GL_COLOR_ATTACHMENT0);
			frame.bufferTexture(1, depthTexture, GL30.GL_DEPTH_ATTACHMENT);
			
			sceneGraph.destroy();
			sceneGraph.init(loader);
		}
		
		GL11.glPushAttrib(GL11.GL_VIEWPORT_BIT);
			frame.bind();
			
			GL11.glViewport(0, 0, (int)frameX, (int)frameY);
			GL11.glClearColor(0, 0, 0, 0);
			GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
			
			GL11.glCullFace(GL11.GL_FRONT);
			
			defaultPipeline.use();
			
			sceneGraph.render(defaultPipeline, camera);
			
		GL11.glPopAttrib();
		
		GL11.glCullFace(GL11.GL_BACK);
				
		GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glLoadIdentity();
		GL11.glMatrixMode(GL11.GL_MODELVIEW);
		GL11.glLoadIdentity();
		
		GL11.glDisable(GL11.GL_DEPTH_TEST);
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		GL11.glEnable(GL11.GL_TEXTURE_2D);
		
		GL20.glUseProgram(0);
		Frame.bindNone(GL30.GL_FRAMEBUFFER);
		frame.getTexture(0).bind();
		GL11.glBegin(GL11.GL_QUADS);
			GL11.glVertex2f(-1, -1); GL11.glTexCoord2f(1, 0);
			GL11.glVertex2f( 1, -1); GL11.glTexCoord2f(1, 1);
			GL11.glVertex2f( 1,  1); GL11.glTexCoord2f(0, 1);
			GL11.glVertex2f(-1,  1); GL11.glTexCoord2f(0, 0);
		GL11.glEnd();
	}

	public void update(float deltaTime) throws Throwable {
		sceneGraph.update(deltaTime);
		
		screenBounds = new Rectangle(new Vector2f(CrossLauncher.getInstance().getWindow().getMode().getWidth(), CrossLauncher.getInstance().getWindow().getMode().getHeight()), new Vector3f(0, 0, 0));
	}
	
	public void destroy() throws Throwable {
		sceneGraph.destroy();
	}
	
	public UISceneGraph getSceneGraph(){
		return sceneGraph;
	}
	
	public BitmapFont getBitmapFont(){
		return font;
	}
}
