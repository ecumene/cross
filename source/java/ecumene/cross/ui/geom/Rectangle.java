package ecumene.cross.ui.geom;

import javax.vecmath.Matrix4f;
import javax.vecmath.Vector2f;
import javax.vecmath.Vector3f;

public class Rectangle implements ITransformShape {
	public Vector2f dimension;
	public Vector3f position;
	
	public Rectangle(Vector2f dimension, Vector3f position){
		this.dimension = new Vector2f(dimension);
		this.position  = new Vector3f(position);
	}
	
	@Override
	public Matrix4f getTransform() {
		Matrix4f out = new Matrix4f();
		out.setIdentity();
		Vector3f translation = new Vector3f(position);
		translation.scale(0.5f);
		out.setTranslation(translation);
		return out;
	}
	
	public boolean isColliding(Rectangle b){
		return isColliding(this, b);
	}
	
	public static boolean isColliding(Rectangle a, Rectangle b){
		if(!(a.position.z != b.position.z) && Math.abs(a.position.x - b.position.x) < a.dimension.x + b.dimension.x){
			if(Math.abs(a.position.y - b.position.y) < a.dimension.y + b.dimension.y){
				return true;
			}
		}
		return false;
	}
}
