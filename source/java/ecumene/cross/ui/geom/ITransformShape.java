package ecumene.cross.ui.geom;

import javax.vecmath.Matrix4f;

public interface ITransformShape {
	public Matrix4f getTransform();
}
