package ecumene.cross.ui;

import ecumene.asset.AssetLoader;
import ecumene.opengl.shader.Pipeline;
import ecumene.scene.ISceneGraph;
import ecumene.scene.ISceneNode;

public class UISceneGraph implements ISceneGraph, IUILoopable {
	public UINode root;
	
	public UISceneGraph(UINode root) {
		this.root = root;
	}
	
	@Override
	public ISceneNode getRoot() {
		return root;
	}

	@Override
	public void init(AssetLoader globalAssets) throws Throwable {
		root.init(globalAssets);
	}

	@Override
	public void render(Pipeline pipeline, UICamera camera) throws Throwable {
		root.render(pipeline, camera);
	}

	@Override
	public void update(float deltaTime) throws Throwable {
		root.update(deltaTime);
	}

	@Override
	public void destroy() throws Throwable {
		root.destroy();
	}

}