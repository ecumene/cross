package ecumene.cross.ui;

import ecumene.asset.AssetLoader;
import ecumene.opengl.shader.Pipeline;

public interface IUILoopable {
	public void init(AssetLoader globalAssets)             throws Throwable;
	public void render(Pipeline pipeline, UICamera camera) throws Throwable;
	public void update(float deltaTime)                    throws Throwable;
	public void destroy()                                  throws Throwable;
}
