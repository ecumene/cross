package ecumene.cross.world;

import ecumene.cross.world.scene.WorldComponent;
import ecumene.cross.world.scene.WorldNode;
import ecumene.geom.wfobj.IOBJOGLBatch;
import ecumene.opengl.Frame;
import ecumene.opengl.OpenGLException;
import ecumene.opengl.shader.Pipeline;

public class WorldOBJOGLBatchComponent extends WorldComponent {
	private WorldOBJOGLBatch objBatch;
	
	public WorldOBJOGLBatchComponent(WorldNode parent, IOBJOGLBatch copy) throws OpenGLException {
		super(parent);
		objBatch = new WorldOBJOGLBatch(copy);
	}
	
	@Override
	public void render(Frame frame, Pipeline pipeline) throws Throwable {
		super.render(frame, pipeline);
		objBatch.setCurrentPipeline(pipeline);
		objBatch.render();
	}
	
	public IOBJOGLBatch getOBJOGLBatch(){ return objBatch; }
}
