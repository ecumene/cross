package ecumene.cross.world.gfx;

import java.util.ArrayList;
import java.util.List;

import ecumene.cross.world.WorldGameContext;
import ecumene.cross.world.scene.WorldNode;
import ecumene.opengl.Frame;
import ecumene.opengl.shader.Pipeline;

public class MultiPassObject extends WorldNode {
	
	protected List<Pipeline> passes;
	
	public MultiPassObject(WorldGameContext world) throws Throwable {
		super();
		passes = new ArrayList<Pipeline>();
	}	
	
	@Override
	public void render(Frame f, Pipeline p) throws Throwable {
		super.render(f, p);
		for(int i = 0; i < passes.size(); i++){
			passes.get(i).use();
			render(i, passes.get(i));
			super.render(f, p);
		}
	}
	
	public void render(int passID, Pipeline p){ }
	
	public void addPass(Pipeline p){
		passes.add(p);
	}
	
	public void removePass(Pipeline p){
		passes.remove(p);
	}
	
	public void removePass(int i){
		passes.remove(i);
	}
	
}
