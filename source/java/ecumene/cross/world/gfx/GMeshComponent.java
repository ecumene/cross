package ecumene.cross.world.gfx;

import ecumene.cross.world.scene.WorldComponent;
import ecumene.cross.world.scene.WorldNode;
import ecumene.geom.ogl.OGLGeomBuffer;
import ecumene.opengl.Frame;
import ecumene.opengl.OpenGLException;
import ecumene.opengl.shader.Pipeline;

public class GMeshComponent extends WorldComponent {
	protected OGLGeomBuffer vbo;

	public GMeshComponent(WorldNode parent) {
		super(parent);
		vbo = new OGLGeomBuffer();
	}
	
	@Override
	public void render(Frame f, Pipeline p) throws Throwable {
		super.render(f, p);
		vbo.render();
	}
	
	public void destroy() throws OpenGLException{
		vbo.destroy();
	}
}
