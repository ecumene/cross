package ecumene.cross.world;

import javax.vecmath.Vector2f;

import ecumene.cross.CrossLauncher;
import ecumene.cross.world.scene.WorldNode;

public class WorldInputObject extends WorldNode {
	
	private Vector2f mouseDelta;
	private Vector2f mousePosition;
	
	public WorldInputObject(WorldGameContext world) throws Throwable {
		super();
		mouseDelta = new Vector2f();
		mousePosition = new Vector2f();
	}
	
	@Override
	public void update(float dt) throws Throwable {
		super.update(dt);
		
		mouseDelta.x = (float)CrossLauncher.getInstance().getWindow().getMouse().getDX();
		mouseDelta.y = (float)CrossLauncher.getInstance().getWindow().getMouse().getDX();
		
		mousePosition.x = (float)CrossLauncher.getInstance().getWindow().getMouse().getX();
		mousePosition.y = (float)CrossLauncher.getInstance().getWindow().getMouse().getY();
	}
	
	public boolean isKeyDown(int code){
		return CrossLauncher.getInstance().getWindow().getKeyboard().isPressed(code);
	}
}
