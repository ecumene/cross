package ecumene.cross.world.extras;

import javax.vecmath.Matrix3f;
import javax.vecmath.Vector3f;

import com.bulletphysics.dynamics.RigidBody;
import com.bulletphysics.linearmath.Transform;

import ecumene.cross.action.IAction;
import ecumene.cross.world.dynamic.DynamicComponent;
import ecumene.cross.world.scene.WorldComponent;
import ecumene.cross.world.scene.WorldNode;
import ecumene.opengl.Frame;
import ecumene.opengl.shader.Pipeline;

public class MoveToComponent extends WorldComponent {
	private RigidBody   body;
	private RigidBody   targetBody;
	private Vector3f    target;
	private IAction     action;
	
	public boolean invertVelocity = false;
	public boolean normalize      = false;
	public int scale              = 1;
	
	public MoveToComponent(WorldNode parent, IAction trigger, Vector3f position, DynamicComponent component) {
		super(parent);
		body = component.getDynamicsBody();
		this.action = trigger;
		this.target = position;
	}
	
	public MoveToComponent(WorldNode parent, Vector3f position, DynamicComponent component) {
		this(parent, new IAction(){  
		@Override
		public boolean isTriggered() {
			return true;
		}}, position, component);
	}
	
	public MoveToComponent(WorldNode parent, IAction action, DynamicComponent target, DynamicComponent component) {
		this(parent, action, target.getDynamicsBody().getWorldTransform(new Transform()).origin, component);
		this.targetBody = target.getDynamicsBody();
	}
	
	@Override
	public void render(Frame f, Pipeline p) throws Throwable {
		super.render(f, p);
		Vector3f point = body.getLinearVelocity(new Vector3f());
		Matrix3f mat = body.getWorldTransform(new Transform()).basis;
		mat.invert();
		mat.transform(point);
	}
	
	@Override
	public void update(float dt) throws Throwable {
		super.update(dt);
		
		if(action.isTriggered()){
			Vector3f cameraPos = new Vector3f();
			
			if(targetBody != null){cameraPos = new Vector3f(targetBody.getWorldTransform(new Transform()).origin);}
			else                  {cameraPos = new Vector3f(target);}
						
			Vector3f bodyPos = new Vector3f(body.getWorldTransform(new Transform()).origin);
			Vector3f velocity = new Vector3f(cameraPos);
			bodyPos.negate();
			velocity.add(new Vector3f(bodyPos));
			if(invertVelocity) velocity.negate();
			if(normalize)      velocity.normalize();
			velocity.scale(scale);
			body.setLinearVelocity(velocity);
		}
	}
	
	public Vector3f getTarget() {
		return target;
	}

	public void setTarget(Vector3f target) {
		this.target = target;
	}

	public boolean isInvertVelocity() {
		return invertVelocity;
	}

	public MoveToComponent setInvertVelocity(boolean invertVelocity) {
		this.invertVelocity = invertVelocity;
		return this;
	}

}