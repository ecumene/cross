package ecumene.cross.world.scene;

import java.nio.ByteBuffer;

import javax.vecmath.Matrix4f;
import javax.vecmath.Vector3f;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL14;
import org.lwjgl.opengl.GL30;

import com.bulletphysics.collision.shapes.BoxShape;
import com.bulletphysics.collision.shapes.CollisionShape;
import com.bulletphysics.dynamics.RigidBody;
import com.bulletphysics.dynamics.RigidBodyConstructionInfo;
import com.bulletphysics.linearmath.DefaultMotionState;
import com.bulletphysics.linearmath.MotionState;
import com.bulletphysics.linearmath.Transform;

import ecumene.asset.AssetLoader;
import ecumene.cross.CrossLauncher;
import ecumene.cross.world.IWorldLoopable;
import ecumene.cross.world.WorldDynamicCamera;
import ecumene.cross.world.WorldFPSControlComponent;
import ecumene.cross.world.WorldGameContext;
import ecumene.cross.world.WorldInputObject;
import ecumene.cross.world.dynamic.DynamicComponent;
import ecumene.cross.world.dynamic.ProjectionTransformComponent;
import ecumene.cross.world.dynamic.ViewTransformComponent;
import ecumene.cross.world.light.Light;
import ecumene.cross.world.light.WorldLightBatchObject;
import ecumene.geom.Projection;
import ecumene.opengl.Frame;
import ecumene.opengl.shader.Pipeline;
import ecumene.opengl.shader.ShaderAsset;
import ecumene.opengl.texture.Texture;
import ecumene.scene.ISceneGraph;
import ecumene.scene.ISceneNode;

public class WorldSceneGraph implements ISceneGraph, IWorldLoopable {
	public WorldNode root;
	private WorldGameContext parent;
	
	private Frame    worldFrame;
	private Pipeline postWorld;
	private Pipeline ambient;
	
	private WorldLightBatchObject lightBatcher;
	private WorldDynamicCamera    camera;
	private WorldInputObject 	  input;
	
	public WorldSceneGraph(WorldGameContext parent, WorldNode root) throws Throwable {
		this.root = root;
		this.parent = parent;
		
		Matrix4f startTranslation = new Matrix4f();
		startTranslation.setIdentity();
		startTranslation.setTranslation(new Vector3f(0, 1.9f, 0));
		
		CollisionShape boxShape = new BoxShape(new Vector3f(0.2f, 1.7f, 0.2f));
		MotionState boxMotionState = new DefaultMotionState();
		Vector3f boxInertia = new Vector3f(0, 0, 0);
		boxShape.calculateLocalInertia(2.5f, boxInertia);
		RigidBodyConstructionInfo boxConstructionInfo = new RigidBodyConstructionInfo(0.2f, boxMotionState, boxShape, boxInertia);
		boxConstructionInfo.restitution = .0f;
		boxConstructionInfo.angularDamping = 0.95f;
		boxConstructionInfo.friction = 90.0f;
		RigidBody dynBody = new RigidBody(boxConstructionInfo);
		dynBody.setActivationState(RigidBody.DISABLE_DEACTIVATION);
		dynBody.setWorldTransform(new Transform(startTranslation));
		dynBody.setAngularFactor(0);
		DynamicComponent dynamics = new DynamicComponent(camera, dynBody);
		
		camera = new WorldDynamicCamera(new ProjectionTransformComponent(camera, Projection.createPerspective(WorldGameContext.CAMERA_FOV_DEG, 
						         (float) CrossLauncher.getInstance().getWindow().getMode().getWidth() / CrossLauncher.getInstance().getWindow().getMode().getHeight(), 
						         1000f, 0.01f)),
								 new ViewTransformComponent(camera), 
					             dynamics);
		camera.addComponent(new WorldFPSControlComponent(camera, camera.getViewComponent(), camera.getDynamicComponent()));
		
		root.addChild(camera);
				
		input        = new WorldInputObject(parent);
		lightBatcher = new WorldLightBatchObject(root, parent);
				
		root.addChild(input);
	}
	
	@Override
	public ISceneNode getRoot() {
		return root;
	}

	@Override
	public void init(AssetLoader globalAssets) throws Throwable {
		if(postWorld == null){
			postWorld = new Pipeline();
			postWorld.attachShader((ShaderAsset) parent.getLoader().getAsset("pipeline.post.default-fs"));
			postWorld.attachShader((ShaderAsset) parent.getLoader().getAsset("pipeline.post.default-vs"));
			postWorld.link();
		}
		
		if(ambient == null){
			ambient = new Pipeline();
			ambient.attachShader((ShaderAsset)parent.getLoader().getAsset("pipeline.world.pass.ambient-fs"));
			ambient.attachShader((ShaderAsset)parent.getLoader().getAsset("pipeline.world.pass.ambient-vs"));
			ambient.link();
		}
		
		root.init(globalAssets);
	}

	@Override
	public void render(Frame frame, Pipeline pipeline) throws Throwable {
		if(CrossLauncher.getInstance().getWindow().resized()){
			if(worldFrame != null) {worldFrame.destroy(true);}
				
			Texture colorTexture = new Texture(GL11.GL_TEXTURE_2D, 0, GL11.GL_LINEAR); 
			colorTexture.bind();
			GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, GL11.GL_RGB16, (int)(CrossLauncher.getInstance().getWindow().getMode().getWidth() * WorldGameContext.WINDOW_MAP_MUL), (int)(CrossLauncher.getInstance().getWindow().getMode().getHeight() * WorldGameContext.WINDOW_MAP_MUL), 0, GL11.GL_RGB, GL11.GL_FLOAT, (ByteBuffer) null);
			
			Texture depthTexture = new Texture(GL11.GL_TEXTURE_2D, 0, GL11.GL_LINEAR);
			depthTexture.bind();
			GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, GL14.GL_DEPTH_COMPONENT32, (int)(CrossLauncher.getInstance().getWindow().getMode().getWidth() * WorldGameContext.WINDOW_MAP_MUL), (int)(CrossLauncher.getInstance().getWindow().getMode().getHeight() * WorldGameContext.WINDOW_MAP_MUL), 0, GL11.GL_DEPTH_COMPONENT, GL11.GL_FLOAT, (ByteBuffer) null);
			
			worldFrame = new Frame(GL30.GL_FRAMEBUFFER);
			worldFrame.bind();
			worldFrame.bufferTexture(0, colorTexture, GL30.GL_COLOR_ATTACHMENT0);
			worldFrame.bufferTexture(1, depthTexture, GL30.GL_DEPTH_ATTACHMENT);
		}
		
		GL11.glPushAttrib(GL11.GL_VIEWPORT_BIT);
			worldFrame.bind();
					GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
					GL11.glViewport(0, 0, (int)(CrossLauncher.getInstance().getWindow().getMode().getWidth() * WorldGameContext.WINDOW_MAP_MUL), (int)(CrossLauncher.getInstance().getWindow().getMode().getHeight() * WorldGameContext.WINDOW_MAP_MUL));
					
					GL11.glEnable(GL11.GL_DEPTH_TEST);
					GL11.glDisable(GL11.GL_BLEND);
					GL11.glDepthFunc(GL11.GL_LEQUAL);
					
					ambient.use();
					ambient.setUniformf("u_lAmbient", 0.65f, 0.65f, 0.7f);
					root.render(worldFrame, ambient);
					lightBatcher.render(worldFrame, ambient);
	        Frame.bindNone(GL30.GL_FRAMEBUFFER);
        GL11.glPopAttrib();
        
        GL11.glViewport(0, 0, CrossLauncher.getInstance().getWindow().getMode().getWidth(), CrossLauncher.getInstance().getWindow().getMode().getHeight());

		GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glLoadIdentity();
		GL11.glMatrixMode(GL11.GL_MODELVIEW);
		GL11.glLoadIdentity();
		
		postWorld.use();
		postWorld.setUniformf("u_inverseFilterTextureSize", 1.0f / (CrossLauncher.getInstance().getWindow().getMode().getWidth()), 1.0f / (CrossLauncher.getInstance().getWindow().getMode().getHeight()));
		postWorld.setUniformf("u_aaSpanMax",    8.0f);
		postWorld.setUniformf("u_aaReduceMin",  1.0f/512.0f);
		postWorld.setUniformf("u_aaReduceMul",  1.0f/8.0f);
		postWorld.setUniformf("u_resolution",   CrossLauncher.getInstance().getWindow().getMode().getWidth(), CrossLauncher.getInstance().getWindow().getMode().getHeight());
		postWorld.setUniformf("u_vIntensity",   0.8f);
		postWorld.setUniformf("u_vOuterRadius", 1.2f);
		postWorld.setUniformf("u_vInnerRadius", 0.4f);
		GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);	
		
		GL11.glDisable(GL11.GL_DEPTH_TEST);
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		
		postWorld.setUniformi("u_sColor0", 0);
		
		worldFrame.getTexture(0).bind();
		GL11.glBegin(GL11.GL_QUADS);
			GL11.glVertex2f(-1, -1); GL11.glTexCoord2f(1, 0);
			GL11.glVertex2f( 1, -1); GL11.glTexCoord2f(1, 1);
			GL11.glVertex2f( 1,  1); GL11.glTexCoord2f(0, 1);
			GL11.glVertex2f(-1,  1); GL11.glTexCoord2f(0, 0);
		GL11.glEnd();
	}
	
	public WorldDynamicCamera getCamera(){
		return camera;
	}
	
	public void addLight(Light l) throws Throwable{
		lightBatcher.addLight(l);
	}
	
	public WorldLightBatchObject getLightBatcher(){
		return lightBatcher;
	}
	
	public WorldInputObject getInput(){
		return input;
	}
	
	@Override
	public void update(float deltaTime) throws Throwable {
		root.update(deltaTime);
	}

	@Override
	public void destroy() throws Throwable {		
		root.destroy();
	}
}
