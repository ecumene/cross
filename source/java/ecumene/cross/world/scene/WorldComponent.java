package ecumene.cross.world.scene;

import ecumene.asset.AssetLoader;
import ecumene.cross.world.IWorldLoopable;
import ecumene.opengl.Frame;
import ecumene.opengl.shader.Pipeline;
import ecumene.scene.ISceneComponent;

public class WorldComponent implements ISceneComponent, IWorldLoopable {
	public WorldNode parent;
	
	public WorldComponent(WorldNode parent){
		this.parent = parent;
	}
	
	public WorldNode getParent(){
		return parent;
	}

	@Override public void init(AssetLoader globalAssets)         throws Throwable { }
	@Override public void render(Frame frame, Pipeline pipeline) throws Throwable { }
    @Override public void update(float deltaTime)                throws Throwable { }
	@Override public void destroy()                              throws Throwable { }
}
