package ecumene.cross.world.scene;

import java.util.ArrayList;
import java.util.List;

import javax.vecmath.Matrix4f;

import ecumene.EcuException;
import ecumene.asset.AssetLoader;
import ecumene.cross.CrossLauncher;
import ecumene.cross.world.IWorldLoopable;
import ecumene.cross.world.WorldGameContext;
import ecumene.opengl.Frame;
import ecumene.opengl.shader.Pipeline;
import ecumene.scene.ISceneComponent;
import ecumene.scene.ISceneGraph;
import ecumene.scene.ISceneNode;

public class WorldNode implements ISceneNode, IWorldLoopable {
	public WorldGameContext world;
	public WorldNode        parent;
	public Matrix4f         object;
	
	public List<ISceneComponent> components;
	public List<ISceneNode>      children;
	
	public WorldNode(){
		object = new Matrix4f();
		object.setIdentity();
		children   = new ArrayList<ISceneNode>();
		components = new ArrayList<ISceneComponent>();
	}
	
	public WorldGameContext getWorld(){
		return ((WorldGameContext) CrossLauncher.getInstance().getContext());
	}
	
	public Matrix4f getTransform(){
		if(parent != null && parent.getTransform() != null){
			Matrix4f out = new Matrix4f(parent.getTransform()); 
			out.setIdentity();
			out.mul(object);
			return out;
		} else return object;	
	}
	
	public Matrix4f getPrivateTransform(){
		return object;
	}

	@Override
	public ISceneGraph getScene() {
		return world.getSceneGraph();
	}

	@Override
	public ISceneNode getParent() {
		return parent;
	}

	@Override
	public void addChild(ISceneNode node) {
		if(!(node instanceof WorldNode)) 
			throw new EcuException("Given scene node must be a world node");
		((WorldNode) node).parent = this;
		children.add((WorldNode) node);
	}

	@Override
	public void addComponent(ISceneComponent node) {
		if(!(node instanceof WorldComponent)) 
			throw new EcuException("Given node component must be a world component");
		((WorldComponent) node).parent = this;
		components.add((WorldComponent) node);
	}

	@Override
	public List<ISceneNode> getChildren() {
		return children;
	}

	@Override
	public List<ISceneComponent> getComponents() {
		return components;
	}

	@Override
	public void init(AssetLoader globalAssets) throws Throwable {
		for(int i = 0; i < components.size(); i++){
			((WorldComponent) components.get(i)).init(globalAssets);
		}
		
		for(int i = 0; i < children.size(); i++){
			((WorldNode) children.get(i)).init(globalAssets);
		}
	}

	@Override
	public void render(Frame frame, Pipeline pipeline) throws Throwable {
		for(int i = 0; i < components.size(); i++){
			((WorldComponent) components.get(i)).render(frame, pipeline);
		}
		
		for(int i = 0; i < children.size(); i++){
			((WorldNode) children.get(i)).render(frame, pipeline);
		}
	}

	@Override
	public void update(float deltaTime) throws Throwable {
		for(int i = 0; i < components.size(); i++){
			((WorldComponent) components.get(i)).update(deltaTime);
		}
		
		for(int i = 0; i < children.size(); i++){
			((WorldNode) children.get(i)).update(deltaTime);
		}
	}

	@Override
	public void destroy() throws Throwable {
		for(int i = 0; i < components.size(); i++){
			((WorldComponent) components.get(i)).destroy();
		}
		
		for(int i = 0; i < children.size(); i++){
			((WorldNode) children.get(i)).destroy();
		}
	}
}
