package ecumene.cross.world;

import ecumene.geom.ogl.OGLGeomBuffer;
import ecumene.geom.wfobj.IOBJOGLBatch;
import ecumene.geom.wfobj.OBJOGLBatch;
import ecumene.geom.wfobj.OBJOGLFaceGroup;
import ecumene.opengl.OpenGLException;
import ecumene.opengl.shader.Pipeline;

public class WorldOBJOGLBatch extends OBJOGLBatch {
	private Pipeline currentPipeline;
	
	public WorldOBJOGLBatch(){ super(); }
	public WorldOBJOGLBatch(IOBJOGLBatch copy) throws OpenGLException{
		this();
		setFaceGroups(copy.getFaceGroups());
	}
	
	@Override
	public void renderFace(OBJOGLFaceGroup face, OGLGeomBuffer bufferer) throws OpenGLException {
		currentPipeline.setUniformf("u_material.specularPower", face.getMaterial().specularPower);
		currentPipeline.setUniformc4("u_material.Ka", face.getMaterial().Ka);
		currentPipeline.setUniformc4("u_material.Kd", face.getMaterial().Kd);
		currentPipeline.setUniformc4("u_material.Ks", face.getMaterial().Ks);
		
		currentPipeline.setUniformi("u_material.map_Kd", 0);
		currentPipeline.setUniformi("u_material.map_Kn", 1);
		currentPipeline.setUniformi("u_material.map_Ks", 2);
		
		face.getMaterial().map_Ka.bind();
		face.getMaterial().map_Ks.bind();
		face.getMaterial().map_Kd.bind();
	}
	
	public void setCurrentPipeline(Pipeline pipeline){
		this.currentPipeline = pipeline;
	}
} 