package ecumene.cross.world.dynamic;

import javax.vecmath.Matrix3f;

import ecumene.cross.world.scene.WorldNode;
import ecumene.opengl.Frame;
import ecumene.opengl.shader.Pipeline;

public class ObjectTransformComponent extends TransformComponent {
	
	public ObjectTransformComponent(WorldNode parent) {
		super(parent);
	}

	@Override
	public void render(Frame f, Pipeline p) throws Throwable{
		super.render(f, p);
		p.setUniformM4("u_tObject", true, getTransform());
		Matrix3f normalMatrix = new Matrix3f(transform.m00, transform.m01,
				transform.m02, transform.m10, transform.m11, transform.m12,
				transform.m20, transform.m21, transform.m22);
		normalMatrix.invert();
		normalMatrix.transpose();
		p.setUniformM3("u_nObject", true, normalMatrix);
	}
}
