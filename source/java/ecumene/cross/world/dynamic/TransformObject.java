package ecumene.cross.world.dynamic;

import ecumene.cross.world.scene.WorldNode;

public abstract class TransformObject extends WorldNode {
	
	public TransformObject() throws Throwable { super(); }
	public TransformObject(TransformObject parent) throws Throwable{
		this();
		getTransformComponent().setParent(parent.getTransformComponent());
	}
	
	public abstract TransformComponent getTransformComponent();
}
