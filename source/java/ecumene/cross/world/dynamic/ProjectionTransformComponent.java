package ecumene.cross.world.dynamic;

import javax.vecmath.Matrix4f;

import ecumene.cross.world.scene.WorldNode;
import ecumene.opengl.Frame;
import ecumene.opengl.shader.Pipeline;

public class ProjectionTransformComponent extends TransformComponent {
	
	public ProjectionTransformComponent(WorldNode parent, Matrix4f transform) {
		super(parent, transform);
	}
	
	@Override
	public void render(Frame f, Pipeline p) throws Throwable{
		super.render(f, p);
		p.setUniformM4("u_tProjection", false, transform);
	}
	
    public static Matrix4f createPerspective(float fov, float aspect, float zNear, float zFar){
    	Matrix4f mat = new Matrix4f();
        mat.setIdentity();
        
        float yScale = 1f / (float) Math.tan(Math.toRadians(fov / 2f));
        float xScale = yScale / aspect;
        float frustumLength = zFar - zNear;
        
        mat.m00 = xScale;
        mat.m11 = yScale;
        mat.m22 = -((zFar + zNear) / frustumLength);
        mat.m23 = -2;
        mat.m32 = ((2 * zFar * zNear) / frustumLength);
        mat.m33 = 0;
        
        return mat;
    }
    
    public static Matrix4f createOrthographic(float left,   float right, float bottom, float top, float zNear,  float zFar){
    	Matrix4f mat = new Matrix4f();
        mat.setIdentity();
        
        mat.m00 = 2 / (right - left);
        mat.m11 = 2 / (top - bottom);
        mat.m22 = -2 / (zFar - zNear);
        mat.m03 = -(right + left) / (right - left);
        mat.m13 = -(top + bottom) / (top - bottom);
        mat.m23 = -(zFar + zNear) / (zFar - zNear);
        mat.m33 = 1;
        
        return mat;
    }
}
