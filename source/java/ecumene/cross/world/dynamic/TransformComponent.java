package ecumene.cross.world.dynamic;

import javax.vecmath.Matrix4f;

import ecumene.cross.world.scene.WorldComponent;
import ecumene.cross.world.scene.WorldNode;

public abstract class TransformComponent extends WorldComponent {
	private TransformComponent transformParent;
	public Matrix4f           transform;
	
	public TransformComponent(WorldNode parent, Matrix4f transform, TransformComponent component) {
		this(parent, transform);
		this.transformParent = component;
	}
	
	public TransformComponent(WorldNode parent, Matrix4f transform){
		super(parent);
		this.transform = transform;
	}
	
	public TransformComponent(WorldNode parent){
		super(parent);
		this.transform = new Matrix4f();
		this.transform.setIdentity();
	}
		
	protected void setParent(TransformComponent component){
		this.transformParent = component;
	}
	
	public TransformComponent getTransformParent(){
		return transformParent;
	}
	
	public void setTransform(Matrix4f transform){
		this.transform = transform;
	}
	
	public Matrix4f getTransform(){
		return transform;
	}
}
