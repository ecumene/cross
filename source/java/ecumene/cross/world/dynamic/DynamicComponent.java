package ecumene.cross.world.dynamic;

import com.bulletphysics.dynamics.RigidBody;
import com.bulletphysics.dynamics.RigidBodyConstructionInfo;

import ecumene.asset.AssetLoader;
import ecumene.cross.world.scene.WorldComponent;
import ecumene.cross.world.scene.WorldNode;

public class DynamicComponent extends WorldComponent {

	private RigidBody dynamicBody;
	
	public DynamicComponent(WorldNode parent){
		super(parent);
	}
	
	public DynamicComponent(WorldNode parent, RigidBodyConstructionInfo dynamicInfo) {
		this(parent, new RigidBody(dynamicInfo));
	}
	
	public DynamicComponent(WorldNode parent, RigidBody dynamicBody) {
		super(parent);
		this.dynamicBody = dynamicBody; 
	}
	
	@Override
	public void init(AssetLoader globalAssets) throws Throwable {
		super.init(globalAssets);
		getParent().getWorld().addRigidBody(getParent(), dynamicBody);
	}
	
	public RigidBody getDynamicsBody(){
		return dynamicBody;
	}
}
