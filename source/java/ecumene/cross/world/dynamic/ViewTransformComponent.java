package ecumene.cross.world.dynamic;

import javax.vecmath.Matrix3f;
import javax.vecmath.Matrix4f;
import javax.vecmath.Vector3f;

import ecumene.cross.world.scene.WorldNode;
import ecumene.opengl.Frame;
import ecumene.opengl.shader.Pipeline;

public class ViewTransformComponent extends TransformComponent {

	public ViewTransformComponent(WorldNode parent) {
		super(parent);
	}

	@Override
	public void render(Frame f, Pipeline p) throws Throwable{
		super.render(f, p);
		p.setUniformM4("u_tView", true, transform);
		Matrix3f normalMatrix = new Matrix3f(transform.m00, transform.m01, transform.m02,
				                             transform.m10, transform.m11, transform.m12,
											 transform.m20, transform.m21, transform.m22);
		normalMatrix.invert();
		p.setUniformM3("u_nView", false, normalMatrix);
	}
	
	public static Matrix4f lookat(Vector3f e, Vector3f c, Vector3f up) {
		Matrix4f out = new Matrix4f();
		out.setIdentity();
		
		Vector3f F = new Vector3f(e);
		F.negate(c);
		F.normalize();
		
		Vector3f U = new Vector3f(up);
		U.normalize();
		
		Vector3f S = new Vector3f();
		S.cross(F, U);
		U.cross(S, F);
		
		out.m00 = S.x;
		out.m10 = S.y;
		out.m20 = S.z;
		
		out.m01 = U.x;
		out.m11 = U.y;
		out.m21 = U.z;
		
		out.m02 = -F.x;
		out.m12 = -F.y;
		out.m22 = -F.z;
						
		Matrix4f translation = new Matrix4f();
		translation.setIdentity();
		translation.setTranslation(new Vector3f(-e.x, -e.y, -e.z));
		out.mul(translation);
		out.transpose();
		
		return out;
	}
}
