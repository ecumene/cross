package ecumene.cross.world.dynamic;

import javax.vecmath.Matrix4f;

import com.bulletphysics.linearmath.Transform;

import ecumene.asset.AssetLoader;
import ecumene.cross.world.scene.WorldComponent;
import ecumene.cross.world.scene.WorldNode;
import ecumene.opengl.Frame;
import ecumene.opengl.shader.Pipeline;

public class DynamicTransformComponent extends WorldComponent {
	public DynamicComponent dynamicComponent;
	public TransformComponent transformComponent;
	
	public DynamicTransformComponent(WorldNode parent, DynamicComponent dynamicComponent, TransformComponent transformComponent) {
		super(parent);
		this.transformComponent = transformComponent;
		this.dynamicComponent = dynamicComponent;
	}
	
	@Override
	public void init(AssetLoader globalAssets) throws Throwable {
		super.init(globalAssets);
		dynamicComponent.init(globalAssets);
		transformComponent.init(globalAssets);
	}
	
	@Override
	public void render(Frame f, Pipeline p) throws Throwable {
		dynamicComponent.render(f, p);
		transformComponent.setTransform(dynamicComponent.getDynamicsBody().getWorldTransform(new Transform()).getMatrix(new Matrix4f()));
		transformComponent.render(f, p);
	}	
	
	@Override
	public void update(float dt) throws Throwable {
		super.update(dt);
		transformComponent.update(dt);
		dynamicComponent.update(dt);
	}
	
	@Override
	public void destroy() throws Throwable {
		super.destroy();
		transformComponent.destroy();
		dynamicComponent.destroy();
	}
	
	public DynamicComponent getDynamicComponent(){
		return dynamicComponent;
	}
	
	public TransformComponent getTransformComponent(){
		return transformComponent;
	}
}
