package ecumene.cross.world;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import javax.vecmath.Matrix4f;
import javax.vecmath.Quat4f;
import javax.vecmath.Vector3f;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;

import com.bulletphysics.collision.broadphase.BroadphaseInterface;
import com.bulletphysics.collision.broadphase.DbvtBroadphase;
import com.bulletphysics.collision.dispatch.CollisionConfiguration;
import com.bulletphysics.collision.dispatch.CollisionDispatcher;
import com.bulletphysics.collision.dispatch.DefaultCollisionConfiguration;
import com.bulletphysics.collision.shapes.BoxShape;
import com.bulletphysics.collision.shapes.CollisionShape;
import com.bulletphysics.collision.shapes.SphereShape;
import com.bulletphysics.collision.shapes.StaticPlaneShape;
import com.bulletphysics.dynamics.DiscreteDynamicsWorld;
import com.bulletphysics.dynamics.DynamicsWorld;
import com.bulletphysics.dynamics.RigidBody;
import com.bulletphysics.dynamics.RigidBodyConstructionInfo;
import com.bulletphysics.dynamics.constraintsolver.ConstraintSolver;
import com.bulletphysics.dynamics.constraintsolver.SequentialImpulseConstraintSolver;
import com.bulletphysics.linearmath.DefaultMotionState;
import com.bulletphysics.linearmath.MotionState;
import com.bulletphysics.linearmath.Transform;

import ecumene.asset.AssetLoader;
import ecumene.cross.CrossGameContext;
import ecumene.cross.CrossLauncher;
import ecumene.cross.action.MouseClickAction;
import ecumene.cross.common.JBulletRigidBodyFactory;
import ecumene.cross.ui.UIContext;
import ecumene.cross.world.dynamic.DynamicComponent;
import ecumene.cross.world.dynamic.DynamicTransformComponent;
import ecumene.cross.world.dynamic.ObjectTransformComponent;
import ecumene.cross.world.extras.MoveToComponent;
import ecumene.cross.world.light.Attenuation;
import ecumene.cross.world.light.SpotLight;
import ecumene.cross.world.light.WorldSpotLightComponent;
import ecumene.cross.world.scene.WorldNode;
import ecumene.cross.world.scene.WorldSceneGraph;
import ecumene.data.Color;
import ecumene.geom.wfobj.OBJOGLAsset;
import ecumene.logging.ILogger;
import ecumene.opengl.shader.LoggedShaderAsset;
import ecumene.opengl.texture.ImageAsset;

public class WorldGameContext extends CrossGameContext {
	
	public static float GRAVITY_MUL    = 1.0f;
	public static float WINDOW_MAP_MUL = 1.0f;
	public static float SHADOW_MAP_MUL = 4.0f;
	public static boolean CAMERA_REACT_VEL = true;
	public static float CAMERA_FOV_DEG = 50.0f;
	private static float LAST_GRAV_MUL = -1;
	
	private AssetLoader loader;
	
	private DynamicsWorld dynWorld;
	private UIContext     uiOverlay;
	
	private WorldSceneGraph            scene;
	private Map<WorldNode, RigidBody>  bodies; // describes the relation between world nodes & rigid bodies
	
	public WorldGameContext(CrossLauncher parent) throws Throwable {
		super(parent);		
		getLaucher();
		ILogger logging = CrossLauncher.getLogger();
		loader = new AssetLoader();
		getLoader().add("pipeline.world.pass.ambient-fs",  new LoggedShaderAsset(logging, true, new File("./asset/shader/world/pass-ambient.fs"), GL20.GL_FRAGMENT_SHADER));
		getLoader().add("pipeline.world.pass.ambient-vs",  new LoggedShaderAsset(logging, true, new File("./asset/shader/world/pass-ambient.vs"), GL20.GL_VERTEX_SHADER));
		getLoader().add("pipeline.ui.default-fs",          new LoggedShaderAsset(logging, true, new File("./asset/shader/ui/default.fs"), GL20.GL_FRAGMENT_SHADER));
		getLoader().add("pipeline.ui.default-vs",          new LoggedShaderAsset(logging, true, new File("./asset/shader/ui/default.vs"), GL20.GL_VERTEX_SHADER));
		getLoader().add("pipeline.world.spotlight-vs",     new LoggedShaderAsset(logging, true, new File("./asset/shader/world/light/pass-forward-spotlight.vs"), GL20.GL_VERTEX_SHADER));
		getLoader().add("pipeline.world.spotlight-fs",     new LoggedShaderAsset(logging, true, new File("./asset/shader/world/light/pass-forward-spotlight.fs"), GL20.GL_FRAGMENT_SHADER));
		getLoader().add("pipeline.world.depthfill.2D-vs",  new LoggedShaderAsset(logging, true, new File("./asset/shader/world/depthfill/depthfill-2D.vs"), GL20.GL_VERTEX_SHADER));
		getLoader().add("pipeline.world.depthfill.2D-fs",  new LoggedShaderAsset(logging, true, new File("./asset/shader/world/depthfill/depthfill-2D.fs"), GL20.GL_FRAGMENT_SHADER));
		getLoader().add("pipeline.world.depthfill.cube-vs",new LoggedShaderAsset(logging, true, new File("./asset/shader/world/depthfill/depthfill-cube.vs"), GL20.GL_VERTEX_SHADER));
		getLoader().add("pipeline.world.depthfill.cube-fs",new LoggedShaderAsset(logging, true, new File("./asset/shader/world/depthfill/depthfill-cube.fs"), GL20.GL_FRAGMENT_SHADER));
		getLoader().add("pipeline.post.default-vs",        new LoggedShaderAsset(logging, true, new File("./asset/shader/post/post-default.vs"), GL20.GL_VERTEX_SHADER));
		getLoader().add("pipeline.post.default-fs",        new LoggedShaderAsset(logging, true, new File("./asset/shader/post/post-vig-fxaa.fs"), GL20.GL_FRAGMENT_SHADER));
		getLoader().add("image.font.pixmix",               new ImageAsset(new File("./asset/font/pixmix.png"), 0, GL11.GL_NEAREST, GL11.GL_REPEAT));
		getLoader().add("model.world.static.box",          new OBJOGLAsset(new File("./asset/model/box/box.obj"),       new WorldOBJOGLBatch()));
		getLoader().add("model.world.static.ball",         new OBJOGLAsset(new File("./asset/model/ball/ball.obj"),     new WorldOBJOGLBatch()));
		getLoader().add("model.world.static.monkey",       new OBJOGLAsset(new File("./asset/model/monkey/monkey.obj"), new WorldOBJOGLBatch()));
		getLoader().add("model.world.static.square",       new OBJOGLAsset(new File("./asset/model/plane/plane.obj"),   new WorldOBJOGLBatch()));
		getLoader().add("model.world.static.terminal",     new OBJOGLAsset(new File("./asset/model/terminal/terminal.obj"),   new WorldOBJOGLBatch()));
		loader.load();
		
		BroadphaseInterface broadphase = new DbvtBroadphase();
        CollisionConfiguration collisionConfiguration = new DefaultCollisionConfiguration();
        CollisionDispatcher dispatcher = new CollisionDispatcher(collisionConfiguration);
        ConstraintSolver solver = new SequentialImpulseConstraintSolver();
        dynWorld = new DiscreteDynamicsWorld(dispatcher, broadphase, solver, collisionConfiguration);
        dynWorld.setGravity(new Vector3f(0, -9.80665f, 0));
        
		uiOverlay = new UIContext(null);
		scene     = new WorldSceneGraph(this, new WorldNode());
		
//		UINode boundedTest = new UINode(){
//			@Override
//			public void render(Pipeline pipeline, UICamera camera)
//					throws Throwable {
//				super.render(pipeline, camera);
//			}	
//			@Override
//			public Rectangle getBounding() {
//				return new Rectangle(new Vector2f(Launcher.getInstance().getWindow().getMode().getWidth(), 100f), new Vector3f(0f, 0, 0));
//			}
//		};
//		boundedTest.addComponent(new UIBackgroundComponent(boundedTest, new Color(0, 0, 0, .5f), null));
//		UIButton pressMe = new UIButton("Press Me! Test", uiOverlay.getBitmapFont(), 10, new Vector3f(160f, 50f, 0));
//		boundedTest.addChild(pressMe);
//		uiOverlay.getSceneGraph().getRoot().addChild(boundedTest);
		
		{
			Matrix4f boxInitTransform = new Matrix4f();
			boxInitTransform.setIdentity();
			boxInitTransform.rotY((float) Math.toRadians(180));
			boxInitTransform.setTranslation(new Vector3f(0, 5, 5));
			WorldNode ball = new WorldNode();
			BoxShape sphere = new BoxShape(new Vector3f(1, 1, 1));
			sphere.calculateLocalInertia(2.5f, new Vector3f());
			RigidBodyConstructionInfo constructionInfo = new RigidBodyConstructionInfo(2.5f, new DefaultMotionState(), sphere);
			DynamicTransformComponent transform = new DynamicTransformComponent(ball, new DynamicComponent(ball, JBulletRigidBodyFactory.createRigidBody(constructionInfo, boxInitTransform)), new ObjectTransformComponent(ball));
			ball.addComponent(transform);
			ball.addComponent(new WorldOBJOGLBatchComponent(ball, ((OBJOGLAsset) loader.getAsset("model.world.static.terminal")).getBatch()));
			ball.addComponent(new MoveToComponent(ball, new MouseClickAction(0, true), scene.getCamera().getDynamicComponent(), transform.getDynamicComponent()));
			ball.addComponent(new MoveToComponent(ball, new MouseClickAction(1, true), scene.getCamera().getDynamicComponent(), transform.getDynamicComponent()).setInvertVelocity(true));
			scene.getRoot().addChild(ball);
		}

		{
			Matrix4f ballInitTransform = new Matrix4f();
			ballInitTransform.setIdentity();
			ballInitTransform.setTranslation(new Vector3f(0, 5, -5));
			WorldNode ball = new WorldNode();
			SphereShape sphere = new SphereShape(1f);
			sphere.calculateLocalInertia(2.5f, new Vector3f());
			RigidBodyConstructionInfo constructionInfo = new RigidBodyConstructionInfo(2.5f, new DefaultMotionState(), sphere);
			DynamicTransformComponent transform = new DynamicTransformComponent(ball, new DynamicComponent(ball, JBulletRigidBodyFactory.createRigidBody(constructionInfo, ballInitTransform)), new ObjectTransformComponent(ball));
			WorldSpotLightComponent lightComponent = new WorldSpotLightComponent(ball, transform.getTransformComponent(), new Vector3f(0, 0, -1), new SpotLight(scene, 0.45f, new Color(.1f, .8f, 1, 1), new Attenuation(0, 0f, 0.115f)));
			lightComponent.getSpotLight().color.a = 1.0f;
			lightComponent.getSpotLight().setCastShadow(true);
			ball.addComponent(lightComponent);
			ball.addComponent(transform);
			ball.addComponent(new WorldOBJOGLBatchComponent(ball, ((OBJOGLAsset) loader.getAsset("model.world.static.ball")).getBatch()));
			ball.addComponent(new MoveToComponent(ball, new MouseClickAction(0, true), scene.getCamera().getDynamicComponent(), transform.getDynamicComponent()));
			ball.addComponent(new MoveToComponent(ball, new MouseClickAction(1, true), scene.getCamera().getDynamicComponent(), transform.getDynamicComponent()).setInvertVelocity(true));
			scene.getRoot().addChild(ball);
		}
		
		{
			Matrix4f groundInitTransform = new Matrix4f();
			groundInitTransform.setIdentity();
			groundInitTransform.setTranslation(new Vector3f(0, 0, 0));
			WorldNode ground = new WorldNode();
	        CollisionShape groundShape = new StaticPlaneShape(new Vector3f(0, 1, 0), 0.25f);
	        MotionState groundMotionState = new DefaultMotionState(new Transform(new Matrix4f(new Quat4f(0, 0, 0, 1), new Vector3f(0, 0, 0), 1.0f)));
			RigidBodyConstructionInfo constructionInfo = new RigidBodyConstructionInfo(0, groundMotionState, groundShape);
			DynamicTransformComponent transform = new DynamicTransformComponent(ground, new DynamicComponent(ground, JBulletRigidBodyFactory.createRigidBody(constructionInfo)), new ObjectTransformComponent(ground));
			ground.addComponent(transform);
			ground.addComponent(new WorldOBJOGLBatchComponent(ground, ((OBJOGLAsset) loader.getAsset("model.world.static.square")).getBatch()));
			scene.getRoot().addChild(ground);
		}
		
		bodies = new HashMap<WorldNode, RigidBody>();
	}
	
	@Override
	public void init() throws Throwable {
		super.init();
		uiOverlay.init(loader);

		scene.init(getLoader());
		
		GL11.glClearDepth(1.0f);
		
		GL11.glFrontFace(GL11.GL_CCW);
		GL11.glEnable(GL11.GL_CULL_FACE);
		GL11.glCullFace(GL11.GL_BACK);
		
		GL11.glEnable(GL11.GL_TEXTURE_2D);
		GL11.glDepthRange(0.0f, 1.0f);
	}
	
	@Override
	public void render() throws Throwable {
		super.render();
		GL11.glClearColor(0.45f, 0.45f, 0.6f, 1f);
		scene.render(null, null);
		uiOverlay.render();
	}
	
	@Override
	public void update(float dt) throws Throwable {
		super.update(dt);		
		if(LAST_GRAV_MUL != GRAVITY_MUL){
			Vector3f gravity = new Vector3f();
			getDynWorld().getGravity(gravity);
			gravity.scale(GRAVITY_MUL);
			getDynWorld().setGravity(gravity);
		}
		LAST_GRAV_MUL = GRAVITY_MUL;

		dynWorld.stepSimulation(dt);
				
		scene.update(dt);
		uiOverlay.update(dt);
	}
	
	public DynamicsWorld getDynWorld(){
		return dynWorld;
	}
	
	public void addRigidBody(WorldNode node, RigidBody body){
		bodies.put(node, body);
		dynWorld.addRigidBody(body);
	}
	
	public AssetLoader getLoader(){
		return loader;
	}
	
	@Override
	public void destroy() throws Throwable {
		super.destroy();
		loader.unload();
		scene.destroy();
		uiOverlay.destroy();
	}
	
	public WorldSceneGraph getSceneGraph(){
		return (WorldSceneGraph) scene;
	}
	
	public UIContext getUIContext(){
		return uiOverlay;
	}
}
