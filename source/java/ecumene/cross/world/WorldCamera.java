package ecumene.cross.world;

import ecumene.cross.world.dynamic.ProjectionTransformComponent;
import ecumene.cross.world.dynamic.ViewTransformComponent;
import ecumene.cross.world.scene.WorldNode;

public class WorldCamera extends WorldNode {
	protected ProjectionTransformComponent projectionComponent;
	protected ViewTransformComponent       viewComponent;
		
	public WorldCamera(ProjectionTransformComponent projectionComponent,
					   ViewTransformComponent           viewComponent){
		super();
		this.projectionComponent = projectionComponent;
		this.viewComponent = viewComponent;
				
		addComponent(projectionComponent);
		addComponent(viewComponent);
	}
	
	public ProjectionTransformComponent getProjectionTransformComponent(){
		return projectionComponent;
	}
	
	public ViewTransformComponent getViewComponent(){
		return viewComponent;
	}
}