package ecumene.cross.world;
//package thmedia.cross.world;
//
//import javax.vecmath.Matrix4f;
//import javax.vecmath.Vector3f;
//
//import thmedia.cross.Launcher;
//import thmedia.cross.world.dynamic.DynamicComponent;
//import thmedia.cross.world.dynamic.ProjectionTransformComponent;
//import thmedia.cross.world.dynamic.TransformComponent;
//import thmedia.cross.world.dynamic.ViewTransformComponent;
//import thmedia.cross.world.scene.WorldNode;
//
//import com.bulletphysics.collision.dispatch.CollisionWorld;
//import com.bulletphysics.collision.dispatch.CollisionWorld.RayResultCallback;
//import com.bulletphysics.collision.shapes.BoxShape;
//import com.bulletphysics.collision.shapes.CollisionShape;
//import com.bulletphysics.dynamics.RigidBody;
//import com.bulletphysics.dynamics.RigidBodyConstructionInfo;
//import com.bulletphysics.linearmath.DefaultMotionState;
//import com.bulletphysics.linearmath.MotionState;
//import com.bulletphysics.linearmath.Transform;
//
//import ecumene.geom.Projection;
//import ecumene.glfw.GLFWKeyboard;
//
//public class WorldPlayerCamera extends WorldNode {
//	
//	private ProjectionTransformComponent projectionComponent;
//	private DynamicComponent             dynamicComponent;
//	private TransformComponent           viewComponent;
//	
//	private static float SPEED = 0.6f;
//	private boolean grabbedMouse = true;
//	
//	private Vector3f dynamicPosition;
//	
//	private Vector3f deltaControlVelocity;
//	private Vector3f cameraRotation;
//	
//	public WorldPlayerCamera(WorldGameContext world) throws Throwable {
//
//		deltaControlVelocity = new Vector3f(0, 0, 0);
//		cameraRotation = new Vector3f(0, 0, 0);
//		
//		Matrix4f startTranslation = new Matrix4f();
//		startTranslation.setIdentity();
//		startTranslation.setTranslation(new Vector3f(0, 1.9f, 0));
//		
//		CollisionShape boxShape = new BoxShape(new Vector3f(0.2f, 1.7f, 0.2f));
//        MotionState boxMotionState = new DefaultMotionState();
//        Vector3f boxInertia = new Vector3f(0, 0, 0);
//        boxShape.calculateLocalInertia(2.5f, boxInertia);
//        RigidBodyConstructionInfo boxConstructionInfo = new RigidBodyConstructionInfo(0.2f, boxMotionState, boxShape, boxInertia);
//        boxConstructionInfo.restitution = .0f;
//        boxConstructionInfo.angularDamping = 0.95f;
//        boxConstructionInfo.friction = 90.0f;
//        RigidBody dynBody = new RigidBody(boxConstructionInfo);
//        dynBody.setActivationState(RigidBody.DISABLE_DEACTIVATION);
//        dynBody.setWorldTransform(new Transform(startTranslation));
//        dynBody.setAngularFactor(0);
//        
//		projectionComponent = new ProjectionTransformComponent(this, new Matrix4f());
//		dynamicComponent    = new DynamicComponent(this, dynBody);
//		viewComponent       = new ViewTransformComponent(this);	
//		
//		addComponent(projectionComponent);
//		addComponent(dynamicComponent);
//		addComponent(viewComponent);
//	}
//	
//	private float mouseDX, mouseDY;
//	
//	@Override
//	public void update(float dt) throws Throwable {
//		super.update(dt);
//		
//		if(grabbedMouse){
//			deltaControlVelocity = new Vector3f(0, 0, 0);
//			projectionComponent.transform = Projection.createPerspective(WorldGameContext.CAMERA_FOV_DEG, (float) Launcher.getInstance().getWindow().getMode().getWidth() / Launcher.getInstance().getWindow().getMode().getHeight(), 1000f, 0.01f);
//			dynamicPosition = dynamicComponent.getDynamicsBody().getWorldTransform(new Transform()).origin;
//			
//			RayResultCallback result = new CollisionWorld.ClosestRayResultCallback(new Vector3f(dynamicPosition.x, dynamicPosition.y - 1.68f, dynamicPosition.z), new Vector3f(dynamicPosition.x, -1f, dynamicPosition.z));
//			getWorld().getDynWorld().rayTest(new Vector3f(dynamicPosition.x, dynamicPosition.y - 1.68f, dynamicPosition.z), new Vector3f(dynamicPosition.x, -1f, dynamicPosition.z), result);
//			
//			float frameSpeed = SPEED * dt;
//			
//			if(Launcher.getInstance().getWindow().getMouse().isGrabbed()){
//				float _mouseDX = (float)Launcher.getInstance().getWindow().getMouse().getDX() * 0.0038f;
//				float _mouseDY = (float)Launcher.getInstance().getWindow().getMouse().getDY() * 0.0038f;
//				
//				if(mouseDX == _mouseDX) mouseDX = 0; else mouseDX = _mouseDX;
//				if(mouseDY == _mouseDY) mouseDY = 0; else mouseDY = _mouseDY;
//				
//				cameraRotation.x += mouseDY;
//				cameraRotation.y += mouseDX;
//				
//				if(cameraRotation.x > Math.toRadians(89))  cameraRotation.x = (float) Math.toRadians(89);
//				if(cameraRotation.x < Math.toRadians(-89)) cameraRotation.x = (float) Math.toRadians(-89);
//				mouseDX = _mouseDX; 
//				mouseDY = _mouseDY;
//			}
//			
//			boolean moving = false;
//			if(Launcher.getInstance().getWindow().getKeyboard().isPressed(GLFWKeyboard.KEY_W)){
//				deltaControlVelocity.x += (float) (Math.sin(-cameraRotation.y - 180 * Math.PI / 180) * frameSpeed);
//				deltaControlVelocity.z += (float) (Math.cos(-cameraRotation.y - 180 * Math.PI / 180) * frameSpeed);
//				moving = true;
//			}
//			if(Launcher.getInstance().getWindow().getKeyboard().isPressed(GLFWKeyboard.KEY_S)){
//				deltaControlVelocity.x -= (float) (Math.sin(-cameraRotation.y + 180 * Math.PI / 180) * frameSpeed);
//				deltaControlVelocity.z -= (float) (Math.cos(-cameraRotation.y + 180 * Math.PI / 180) * frameSpeed);
//				moving = true;
//			}
//			if(Launcher.getInstance().getWindow().getKeyboard().isPressed(GLFWKeyboard.KEY_A)){
//				deltaControlVelocity.x += (float) (Math.sin(-cameraRotation.y - 90 * Math.PI / 180) * frameSpeed);
//				deltaControlVelocity.z += (float) (Math.cos(-cameraRotation.y - 90 * Math.PI / 180) * frameSpeed);
//				moving = true;
//			}
//			if(Launcher.getInstance().getWindow().getKeyboard().isPressed(GLFWKeyboard.KEY_D)){
//				deltaControlVelocity.x += (float) (Math.sin(-cameraRotation.y + 90 * Math.PI / 180) * frameSpeed);
//				deltaControlVelocity.z += (float) (Math.cos(-cameraRotation.y + 90 * Math.PI / 180) * frameSpeed);
//				moving = true;
//			}
//			
//			if(result.hasHit()) {			
//				if(result.closestHitFraction <= 0.07){
//					if(Launcher.getInstance().getWindow().getKeyboard().isClicked(GLFWKeyboard.KEY_SPACE)){
//						deltaControlVelocity.y += 6.2f;
//						moving = true;
//					}
//					if(moving) dynamicComponent.getDynamicsBody().setLinearVelocity(deltaControlVelocity);
//				}
//			}
//			
//			Matrix4f view = new Matrix4f();
//			view.setIdentity();
//			Matrix4f rotX = new Matrix4f();
//			Matrix4f rotY = new Matrix4f();
//			Matrix4f tran = new Matrix4f();
//			
//			{			
//				rotX.setIdentity();
//				rotX.rotX(cameraRotation.x);
//				rotY.setIdentity();
//				rotY.rotY(cameraRotation.y);
//				
//				view.mul(rotX);
//				view.mul(rotY);
//				Vector3f dynam_translation = new Vector3f(dynamicPosition);
//				dynam_translation.negate();
//				tran.setIdentity();
//				tran.setTranslation(dynam_translation);
//				view.mul(tran);
//			}
//			
//			viewComponent.setTransform(view);
//		}
//		
//		if(Launcher.getInstance().getWindow().getKeyboard().isClicked(GLFWKeyboard.KEY_TAB)) {
//			grabbedMouse = !grabbedMouse;
//		}
//		
//		Launcher.getInstance().getWindow().getMouse().setGrabbed(grabbedMouse);
//	}
//	
//	public DynamicComponent getDynamicComponent(){
//		return dynamicComponent;
//	}
//	
//	public Vector3f getPosition(){
//		return dynamicPosition;
//	}
//	
//	public Vector3f getRotation(){
//		return cameraRotation;
//	}
//}
