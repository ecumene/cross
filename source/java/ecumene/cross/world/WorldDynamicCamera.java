package ecumene.cross.world;

import ecumene.cross.world.dynamic.DynamicComponent;
import ecumene.cross.world.dynamic.ProjectionTransformComponent;
import ecumene.cross.world.dynamic.ViewTransformComponent;

public class WorldDynamicCamera extends WorldCamera {
	private DynamicComponent dynamicComponent;
	
	public WorldDynamicCamera(ProjectionTransformComponent projectionComponent,
			ViewTransformComponent viewComponent,
			DynamicComponent dynamicComponent){
		super(projectionComponent, viewComponent);
		this.dynamicComponent = dynamicComponent;
		addComponent(dynamicComponent);
	}
	
	public DynamicComponent getDynamicComponent(){
		return dynamicComponent;
	}
}
