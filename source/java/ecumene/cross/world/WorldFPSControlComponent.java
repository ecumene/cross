package ecumene.cross.world;

import javax.vecmath.Matrix4f;
import javax.vecmath.Vector3f;

import com.bulletphysics.collision.dispatch.CollisionWorld;
import com.bulletphysics.collision.dispatch.CollisionWorld.RayResultCallback;
import com.bulletphysics.linearmath.Transform;

import ecumene.cross.CrossLauncher;
import ecumene.cross.world.dynamic.DynamicComponent;
import ecumene.cross.world.dynamic.ViewTransformComponent;
import ecumene.cross.world.scene.WorldComponent;
import ecumene.cross.world.scene.WorldNode;
import ecumene.glfw.GLFWKeyboard;

public class WorldFPSControlComponent extends WorldComponent {

	private ViewTransformComponent viewComponent;
	private DynamicComponent       dynamicComponent;
	
	private static float SPEED = 0.6f;
	private boolean grabbedMouse = true;
	
	private Vector3f dynamicPosition;
	
	private Vector3f deltaControlVelocity;
	private Vector3f cameraRotation;
	
	public WorldFPSControlComponent(WorldNode parent, ViewTransformComponent view, DynamicComponent transformComponent) {
		super(parent);
		this.viewComponent = view;
		this.dynamicComponent = transformComponent;
		
		dynamicPosition = new Vector3f();
		deltaControlVelocity = new Vector3f();
		cameraRotation = new Vector3f();
	}
	
	private float mouseDX, mouseDY;
	
	@Override
	public void update(float deltaTime) throws Throwable {
		super.update(deltaTime);
		
		if(grabbedMouse){
			deltaControlVelocity = new Vector3f(0, 0, 0);
			
			RayResultCallback result = new CollisionWorld.ClosestRayResultCallback(new Vector3f(dynamicPosition.x, dynamicPosition.y - 1.68f, dynamicPosition.z), new Vector3f(dynamicPosition.x, -1f, dynamicPosition.z));
			getParent().getWorld().getDynWorld().rayTest(new Vector3f(dynamicPosition.x, dynamicPosition.y - 1.68f, dynamicPosition.z), new Vector3f(dynamicPosition.x, -1f, dynamicPosition.z), result);
			
			float frameSpeed = SPEED * deltaTime;
			
			if(CrossLauncher.getInstance().getWindow().getMouse().isGrabbed()){
				float _mouseDX = (float)CrossLauncher.getInstance().getWindow().getMouse().getDX() * 0.0038f;
				float _mouseDY = (float)CrossLauncher.getInstance().getWindow().getMouse().getDY() * 0.0038f;
				
				if(mouseDX == _mouseDX) mouseDX = 0; else mouseDX = _mouseDX;
				if(mouseDY == _mouseDY) mouseDY = 0; else mouseDY = _mouseDY;
				
				cameraRotation.x += mouseDY;
				cameraRotation.y += mouseDX;
				
				if(cameraRotation.x > Math.toRadians(89))  cameraRotation.x = (float) Math.toRadians(89);
				if(cameraRotation.x < Math.toRadians(-89)) cameraRotation.x = (float) Math.toRadians(-89);
				mouseDX = _mouseDX; 
				mouseDY = _mouseDY;
			}
			
			boolean moving = false;
			if(CrossLauncher.getInstance().getWindow().getKeyboard().isPressed(GLFWKeyboard.KEY_W)){
				deltaControlVelocity.x += (float) (Math.sin(-cameraRotation.y - 180 * Math.PI / 180) * frameSpeed);
				deltaControlVelocity.z += (float) (Math.cos(-cameraRotation.y - 180 * Math.PI / 180) * frameSpeed);
				moving = true;
			}
			if(CrossLauncher.getInstance().getWindow().getKeyboard().isPressed(GLFWKeyboard.KEY_S)){
				deltaControlVelocity.x -= (float) (Math.sin(-cameraRotation.y + 180 * Math.PI / 180) * frameSpeed);
				deltaControlVelocity.z -= (float) (Math.cos(-cameraRotation.y + 180 * Math.PI / 180) * frameSpeed);
				moving = true;
			}
			if(CrossLauncher.getInstance().getWindow().getKeyboard().isPressed(GLFWKeyboard.KEY_A)){
				deltaControlVelocity.x += (float) (Math.sin(-cameraRotation.y - 90 * Math.PI / 180) * frameSpeed);
				deltaControlVelocity.z += (float) (Math.cos(-cameraRotation.y - 90 * Math.PI / 180) * frameSpeed);
				moving = true;
			}
			if(CrossLauncher.getInstance().getWindow().getKeyboard().isPressed(GLFWKeyboard.KEY_D)){
				deltaControlVelocity.x += (float) (Math.sin(-cameraRotation.y + 90 * Math.PI / 180) * frameSpeed);
				deltaControlVelocity.z += (float) (Math.cos(-cameraRotation.y + 90 * Math.PI / 180) * frameSpeed);
				moving = true;
			}
			
			if(result.hasHit()) {			
				if(result.closestHitFraction <= 0.07){
					if(CrossLauncher.getInstance().getWindow().getKeyboard().isClicked(GLFWKeyboard.KEY_SPACE)){
						deltaControlVelocity.y += 6.2f;
						moving = true;
					}
					if(moving) dynamicComponent.getDynamicsBody().setLinearVelocity(deltaControlVelocity);
				}
			}
		}

		dynamicPosition = dynamicComponent.getDynamicsBody().getWorldTransform(new Transform()).origin;
		
		Matrix4f view = new Matrix4f();
		view.setIdentity();
		Matrix4f rotX = new Matrix4f();
		Matrix4f rotY = new Matrix4f();
		Matrix4f tran = new Matrix4f();
		
		{			
			rotX.setIdentity();
			rotX.rotX(cameraRotation.x);
			rotY.setIdentity();
			rotY.rotY(cameraRotation.y);
			
			view.mul(rotX);
			view.mul(rotY);
			Vector3f dynam_translation = new Vector3f(dynamicPosition);
			dynam_translation.negate();
			tran.setIdentity();
			tran.setTranslation(dynam_translation);
			view.mul(tran);
		}
		
		viewComponent.setTransform(view);
		
		if(CrossLauncher.getInstance().getWindow().getKeyboard().isClicked(GLFWKeyboard.KEY_TAB)) {
			grabbedMouse = !grabbedMouse;
		}
		
		CrossLauncher.getInstance().getWindow().getMouse().setGrabbed(grabbedMouse);
	}
}
