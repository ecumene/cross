package ecumene.cross.world;

import ecumene.asset.AssetLoader;
import ecumene.opengl.Frame;
import ecumene.opengl.shader.Pipeline;

public interface IWorldLoopable {
	public void init(AssetLoader globalAssets)         throws Throwable;
	public void render(Frame frame, Pipeline pipeline) throws Throwable;
	public void update(float deltaTime)                throws Throwable;
	public void destroy()                              throws Throwable;
}
