package ecumene.cross.world.light;

import javax.vecmath.Matrix4f;
import javax.vecmath.Vector3f;

import ecumene.cross.world.WorldGameContext;
import ecumene.cross.world.scene.WorldNode;
import ecumene.cross.world.scene.WorldSceneGraph;
import ecumene.data.Color;
import ecumene.opengl.Frame;
import ecumene.opengl.OpenGLException;
import ecumene.opengl.shader.Pipeline;
import ecumene.opengl.texture.ITexture;

public abstract class Light {
		
	private Attenuation attenuation;
	private float       range;
	private Matrix4f    transform;
	
	public Color    color;
	public Vector3f position;
	public boolean  castShadow;
	
	protected Matrix4f depthProjection;
	protected boolean  individualShadowMap = false;
	
	public Light(WorldSceneGraph root, Color color, Attenuation attenuation) throws Throwable{
		this.color       = color;
		setAttenuation(attenuation);
		
		if(depthProjection == null){
			depthProjection = new Matrix4f();
			depthProjection.setIdentity();
		}
	}
	
	public abstract void init(WorldGameContext world) throws Throwable;
	public abstract void use(Pipeline p, Frame frame) throws OpenGLException;
	
	public abstract Pipeline getProgram();
	public abstract Pipeline getDepthProgram();

	public Attenuation getAttenuation(){
		return attenuation;
	}
	
	public void setAttenuation(Attenuation attenuation){
		this.attenuation = attenuation;
		
		float a = attenuation.getExponent();
		float b = attenuation.getLinear();
		
		this.range = (float)((-b + Math.sqrt(b * b - 4 * a * (attenuation.getConstant() - 256 * color.a * (Math.max(color.r, Math.max(color.g, color.b)))))) / (2 * a));
	}
	
	public void destroy() throws OpenGLException{}
	
	public void setCastShadow(boolean useShadow){
		this.castShadow = useShadow;
	}
	
	public boolean castShadow(){
		return castShadow;
	}
	
	public float getRange(){
		return range;
	}
	
	public Matrix4f getLightMatrix(){
		return transform;
	}
	
	protected void renderDepthMap(WorldNode rootObject) throws Throwable{ }
	
	protected ITexture getShadowMap(){
		return null;
	}
}
