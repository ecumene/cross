package ecumene.cross.world.light;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import javax.vecmath.Matrix4f;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import org.lwjgl.opengl.GL14;
import org.lwjgl.opengl.GL30;

import ecumene.cross.world.WorldGameContext;
import ecumene.cross.world.scene.WorldNode;
import ecumene.opengl.Frame;
import ecumene.opengl.OpenGLException;
import ecumene.opengl.shader.Pipeline;
import ecumene.opengl.texture.Texture;

public class WorldLightBatchObject extends WorldNode {

	public static int SHADOW_MAP_TMUNIT = 4;
	
	private WorldNode   scene;
	private Frame       shadowMap;
	private List<Light> lights;
	
	private static Matrix4f depthBias = new Matrix4f();
	static{
		depthBias.setIdentity();
		depthBias.m00 = 0.5f; depthBias.m01 = 0.0f; depthBias.m02 = 0.0f; depthBias.m03 = 0.0f;
		depthBias.m10 = 0.0f; depthBias.m11 = 0.5f; depthBias.m12 = 0.0f; depthBias.m13 = 0.0f;
		depthBias.m20 = 0.0f; depthBias.m21 = 0.0f; depthBias.m22 = 0.5f; depthBias.m23 = 0.0f;
		depthBias.m30 = 0.5f; depthBias.m31 = 0.5f; depthBias.m32 = 0.5f; depthBias.m33 = 1.0f;
	}
	
	public WorldLightBatchObject(WorldNode scene, WorldGameContext world) throws Throwable {
		super();		
		this.scene  = scene;
		this.lights = new ArrayList<Light>();
		this.addChild(scene);
		
		Texture depthTexture = new Texture(GL11.GL_TEXTURE_2D, SHADOW_MAP_TMUNIT, GL11.GL_NEAREST, GL12.GL_CLAMP_TO_EDGE);
		depthTexture.bind();
		GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, GL14.GL_DEPTH_COMPONENT32, (int)(WorldGameContext.SHADOW_MAP_MUL * 1024), (int)(WorldGameContext.SHADOW_MAP_MUL * 1024), 0, GL11.GL_DEPTH_COMPONENT, GL11.GL_FLOAT, (ByteBuffer) null);
		
		shadowMap = new Frame(GL30.GL_FRAMEBUFFER);
		shadowMap.bind();
		shadowMap.bufferTexture(0, depthTexture, GL30.GL_DEPTH_ATTACHMENT);
	}
	
	@Override
	public void render(Frame frame, Pipeline p) throws Throwable {
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glBlendFunc(GL11.GL_ONE, GL11.GL_ONE); 
		GL11.glDepthFunc(GL11.GL_LEQUAL);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL14.GL_TEXTURE_COMPARE_MODE, GL30.GL_COMPARE_REF_TO_TEXTURE);
		
		for(int i = 0; i < lights.size(); i++){
			Light currLight = lights.get(i);
			Matrix4f depthTransform = null;
			Matrix4f depthBiasedTransform = null;
			
			if(!currLight.individualShadowMap){
				depthTransform = new Matrix4f(currLight.getLightMatrix());
				depthBiasedTransform = new Matrix4f(depthTransform);
				depthBiasedTransform.mul(depthBias);
				
				GL11.glPushAttrib(GL11.GL_VIEWPORT_BIT | GL11.GL_ENABLE_BIT);
				shadowMap.bind();
				{
					GL11.glViewport(0, 0, (int) (WorldGameContext.SHADOW_MAP_MUL * 1024), (int) (WorldGameContext.SHADOW_MAP_MUL * 1024));
					GL11.glClear(GL11.GL_DEPTH_BUFFER_BIT);
							
				    GL11.glEnable(GL11.GL_DEPTH_TEST);
				    GL11.glDepthFunc(GL11.GL_LEQUAL);
				    GL11.glEnable(GL11.GL_CULL_FACE);
				    GL11.glCullFace(GL11.GL_BACK);
				    
				    currLight.getDepthProgram().use();
					currLight.getDepthProgram().setUniformM4("u_tLight", false, depthTransform);
					super.render(shadowMap, currLight.getDepthProgram());
				}
				GL11.glPopAttrib();
			} else {
				currLight.renderDepthMap(getLitScene());
			}
			
			frame.bind();
			GL11.glCullFace(GL11.GL_BACK);
			
			if(!currLight.individualShadowMap) shadowMap.getTexture(0).bind();
			else                               currLight.getShadowMap().bind();
			currLight.getProgram().use();
			currLight.use(currLight.getProgram(), frame);
			currLight.getProgram().setUniformi ("u_sDepthMap", SHADOW_MAP_TMUNIT);
			currLight.getProgram().setUniformi ("u_bShadowMap", currLight.castShadow ? GL11.GL_TRUE : GL11.GL_FALSE);
			if(!currLight.individualShadowMap) currLight.getProgram().setUniformM4("u_tDepthBias", false, depthBiasedTransform);
			super.render(frame, currLight.getProgram());
		}
	}
	
	public WorldNode getLitScene(){
		return scene;
	}
	
	public List<Light> getLights(){
		return lights;
	}
	
	public void addLight(Light l) throws Throwable{
		lights.add(l);
		l.init(getWorld());
	}
	
	public void removeLight(Light l) throws OpenGLException{
		lights.remove(l);
		l.destroy();
	}
	
}
