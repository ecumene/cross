package ecumene.cross.world.light;

import javax.vecmath.Vector4f;

public class Attenuation {
	private Vector4f attenuation;
	
	public Attenuation(int constant, float linear, float exponent){
		attenuation = new Vector4f(constant, linear, exponent, 0);
	}
	
	public float getConstant(){
		return attenuation.x;
	}
	
	public float getLinear(){
		return attenuation.y;
	}
	
	public float getExponent(){
		return attenuation.z;
	}
}
