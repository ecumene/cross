package ecumene.cross.world.light;

import javax.vecmath.Vector3f;

import com.bulletphysics.linearmath.Transform;

import ecumene.asset.AssetLoader;
import ecumene.cross.world.dynamic.TransformComponent;
import ecumene.cross.world.scene.WorldComponent;
import ecumene.cross.world.scene.WorldNode;
import ecumene.opengl.Frame;
import ecumene.opengl.shader.Pipeline;

public class WorldSpotLightComponent extends WorldComponent {

	public  SpotLight          spotLight;
	public  TransformComponent component;
	private Vector3f           direction;
	
	public WorldSpotLightComponent(WorldNode parent, TransformComponent component, Vector3f direction, SpotLight light) {
		super(parent);
		this.spotLight = light;
		this.component = component;
		this.spotLight.direction = direction;
		this.direction = direction;
	}
	
	@Override
	public void init(AssetLoader globalAssets) throws Throwable {
		super.init(globalAssets);
		getParent().getWorld().getSceneGraph().addLight(spotLight);
	}
	
	@Override
	public void render(Frame f, Pipeline p) throws Throwable {
		super.render(f, p);
		spotLight.position  = new Transform(component.getTransform()).origin;
		spotLight.direction = direction;
	}
	
	public SpotLight getSpotLight(){
		return spotLight;
	}
}
