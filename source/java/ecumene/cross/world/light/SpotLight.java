package ecumene.cross.world.light;

import javax.vecmath.Matrix4f;
import javax.vecmath.Vector3f;

import ecumene.cross.world.WorldGameContext;
import ecumene.cross.world.dynamic.ViewTransformComponent;
import ecumene.cross.world.scene.WorldSceneGraph;
import ecumene.data.Color;
import ecumene.geom.Projection;
import ecumene.opengl.Frame;
import ecumene.opengl.OpenGLException;
import ecumene.opengl.shader.Pipeline;
import ecumene.opengl.shader.ShaderAsset;

public class SpotLight extends Light {
	
	private static Pipeline program;
	private static Pipeline depthProgram;
	
	public Vector3f direction;
	public float    cutoff;
	
	public SpotLight(WorldSceneGraph root, float cutoff, Color color, Attenuation attenuation) throws Throwable {
		super(root, color, attenuation);
		this.cutoff = cutoff;
		individualShadowMap = false;
	}

	public void init(WorldGameContext world) throws Throwable {
		if(program == null){
			program = new Pipeline();
			getProgram().attachShader((ShaderAsset) world.getLoader().getAsset("pipeline.world.spotlight-vs"));
			getProgram().attachShader((ShaderAsset) world.getLoader().getAsset("pipeline.world.spotlight-fs"));
			getProgram().link();
		}
		
		if(depthProgram == null){
			depthProgram = new Pipeline();
			depthProgram.attachShader((ShaderAsset) world.getLoader().getAsset("pipeline.world.depthfill.2D-vs"));
			depthProgram.attachShader((ShaderAsset) world.getLoader().getAsset("pipeline.world.depthfill.2D-fs"));
			depthProgram.link();
		}
		
		depthProjection = Projection.createPerspective(120f, 1f, 50f, 0.01f);
	}
	
	@Override
	public void use(Pipeline p, Frame frame) throws OpenGLException {
		p.setUniformf("u_light.pointLight.base.color", color.r, color.g, color.b, color.a);
		p.setUniformf("u_lPosition", position.x, position.y, position.z);
		p.setUniformf("u_lDirection", direction.x, direction.y, direction.z);
		
		p.setUniformf("u_light.pointLight.atten.constant", getAttenuation().getConstant());
		p.setUniformf("u_light.pointLight.atten.linear",   getAttenuation().getLinear());
		p.setUniformf("u_light.pointLight.atten.exponent", getAttenuation().getExponent());
		
		p.setUniformf("u_light.pointLight.range", getRange());
		p.setUniformf("u_light.cutoff", getCutoff());
	}
	
	@Override
	public Matrix4f getLightMatrix() {
		Vector3f up = new Vector3f(0, 1, 0);
		if(!direction.equals(new Vector3f(0.0f, 1.0f, 0.0f)) && !direction.equals(new Vector3f(0.0f, -1.0f, 0.0f))){
		   Vector3f left = new Vector3f();
		   left.cross(direction, up);
		   up.cross(direction, left);
		   left.normalize();
		   up.normalize();
		} else {
		   up = new Vector3f(0, 0, 1);
		}
		Matrix4f out = ViewTransformComponent.lookat(position, direction, up);
		out.mul(depthProjection);
		
		return out;
	}
	
	public float getCutoff(){
		return cutoff;
	}	
	
	@Override
	public Pipeline getDepthProgram() {
		return depthProgram;
	}

	@Override
	public Pipeline getProgram() {
		return program;
	}
		
	@Override
	public void destroy() throws OpenGLException {
		if(program != null){
			program.destroy();
			program = null;
		}
	}
}
