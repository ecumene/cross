package ecumene.cross.action;

public interface IAction {
	public boolean isTriggered();
}
