package ecumene.cross.action;

import ecumene.cross.CrossLauncher;

public class MouseClickAction implements IAction {
	
	private int button;
	public boolean requireGrab;
	
	public MouseClickAction(int button, boolean requireGrab) {
		this.button = button;
		this.requireGrab = requireGrab;
	}
	
	public MouseClickAction(int button){
		this(button, false);
	}

	public int getButton() {
		return button;
	}

	public void setButton(int button) {
		this.button = button;
	}

	@Override
	public boolean isTriggered() {
		return requireGrab ? (CrossLauncher.getInstance().getWindow().getMouse().isGrabbed() && CrossLauncher.getInstance().getWindow().getMouse().isButtonDown(button)) : (CrossLauncher.getInstance().getWindow().getMouse().isButtonDown(button));
	}
	
}
