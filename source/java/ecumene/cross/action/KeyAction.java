package ecumene.cross.action;

import ecumene.cross.CrossLauncher;

public class KeyAction implements IAction {
	public int keycode;
	
	public KeyAction(int keycode){
		this.keycode = keycode;
	}

	@Override
	public boolean isTriggered() {
		return CrossLauncher.getInstance().getWindow().getKeyboard().isPressed(keycode);
	}

	public int getKeycode() {
		return keycode;
	}

	public void setKeycode(int keycode) {
		this.keycode = keycode;
	}
}
