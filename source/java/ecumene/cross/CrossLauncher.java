package ecumene.cross;

import java.io.File;
import java.io.IOException;

import org.lwjgl.glfw.GLFW;

import ecumene.EcuSystem;
import ecumene.cross.world.WorldGameContext;
import ecumene.glfw.GLFWWindow;
import ecumene.glfw.GLFWWindowMode;
import ecumene.logging.Logger;

public class CrossLauncher {
	private static CrossLauncher INSTANCE;
	private static Logger LOGGER;
	
	private String[]   rtArguments;
	private GLFWWindow window;
	private CrossFrameTimer timer;
	private boolean    running;
	
	private CrossGameContext state;
	 
	public CrossLauncher(String[] rtArguments) throws Throwable {
		this.rtArguments = rtArguments;
	}
	
	private void init() throws Throwable {
		GLFWWindow.initializeGLFW(System.err);
		window = new GLFWWindow();
		window.setMode(new GLFWWindowMode("CROSS", 1600, 900, true, false));
		window.create(0);
		window.centerPosition(GLFW.glfwGetPrimaryMonitor());
		window.use();
		
		LOGGER.log("sinfo", "Starting cross game...");
		LOGGER.log("sinfo", "System info: ");
		LOGGER.log("-----", " ... ");
		LOGGER.log("sinfo", "java:   " + EcuSystem.JAVA_VERSION + ", " + EcuSystem.JAVA_VENDOR);
		LOGGER.log("sinfo", "system: " + EcuSystem.OS);
		LOGGER.log("-----", " ... ");
		
		timer = new CrossFrameTimer();
		
		setContext(new WorldGameContext(this));
		
		running = true;
		while(running){
			window.swapAndPoll(); 

			running = !window.isClosing();
			
			window.getKeyboard().startEventFrame();
			update(timer.stepUpdate());
			window.getKeyboard().clearEventFrame();
			
			render();
			
			window.post();
			timer.stepFrame();
		}
		
		window.destroy();
		GLFWWindow.terminate();
	}
	
	private void render() throws Throwable {
		state.render();
	}
	
	private void update(float ms) throws Throwable {
		state.update(ms);
	}
	
	public String[] getRuntimeArguments(){
		return rtArguments;
	}
	
	public static CrossLauncher getInstance(){
		return INSTANCE;
	}
	
	public void stop() throws Throwable{
		getContext().destroy();
		running = false;
	}
	
	public boolean isRunning(){
		return running;
	}
	
	public void setRunning(boolean running){
		this.running = running;
	}
	
	public void setContext(CrossGameContext context) throws Throwable {
		state = context;
		state.init();
	}
	
	public CrossGameContext getContext(){
		return state;
	}
	
	public static Logger getLogger(){
		return LOGGER;
	}
	
	public GLFWWindow getWindow(){
		return window;
	}
	
	public static void main(String[] args){
		try {
			LOGGER = new Logger(new File("./logs/main.log"), true);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		try {
			INSTANCE = new CrossLauncher(args);
			INSTANCE.init();
		} catch (Throwable e) {
			LOGGER.log("strce", "// Beginning stack trace, cause and message may point null");
			LOGGER.log("strce", "{");
			LOGGER.log("strce", "\"" + e.getClass() + "\"" + " says " + "\"" + e.getLocalizedMessage() + "\"");
			for(StackTraceElement element : e.getStackTrace()){
				LOGGER.log("strce", "-   call " + element.getClassName() + "." + element.getMethodName() + "(" + element.getFileName() + ":" + element.getLineNumber() + ")");
			}
			LOGGER.log("strce", "}");
			LOGGER.log("strce", "// End stack trace");
		}
		
		LOGGER.flush();
		System.exit(0);
	}
}
