package ecumene.cross;


public class CrossGameContext {
	private CrossLauncher parent;
	
	public CrossGameContext(CrossLauncher parent){
		this.parent = parent;
	}
	
	public void init() throws Throwable { 
	}
	public void render() throws Throwable {}
	public void update(float dt) throws Throwable {}
	public void destroy() throws Throwable {}
	
	public CrossLauncher getLaucher(){
		return parent;
	}
}
