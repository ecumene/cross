package ecumene.cross.common;

import javax.vecmath.Matrix4f;

public class Camera {
	public Matrix4f projection;
	public Matrix4f view;
	
	public Camera() {
		projection = new Matrix4f(); projection.setIdentity();
		view       = new Matrix4f(); view.setIdentity();
	}
	
	public Matrix4f getProjection(){
		return view;
	}
	
	public Matrix4f getView(){
		return view;
	}
}
