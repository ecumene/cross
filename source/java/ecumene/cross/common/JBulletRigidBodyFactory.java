package ecumene.cross.common;

import javax.vecmath.Matrix4f;

import com.bulletphysics.dynamics.RigidBody;
import com.bulletphysics.dynamics.RigidBodyConstructionInfo;
import com.bulletphysics.linearmath.Transform;

public class JBulletRigidBodyFactory {
	private static Matrix4f IDENTITY = new Matrix4f();
	static { IDENTITY.setIdentity(); }
	
	public static RigidBody createRigidBody(RigidBodyConstructionInfo info, Matrix4f transform, int activationState, int collisionFlag){
		RigidBody body = new RigidBody(info);
		body.setWorldTransform(new Transform(transform));
		body.setActivationState(activationState);
		body.setCollisionFlags(body.getCollisionFlags() | collisionFlag);
		return body;
	}
	
	public static RigidBody createRigidBody(RigidBodyConstructionInfo info, Matrix4f transform, int activation){
		return createRigidBody(info, transform, activation, 0);
	}
	
	public static RigidBody createRigidBody(RigidBodyConstructionInfo info, Matrix4f transform){
		return createRigidBody(info, transform, RigidBody.ACTIVE_TAG, 0);
	}
	
	public static RigidBody createRigidBody(RigidBodyConstructionInfo info){
		return createRigidBody(info, new Matrix4f(IDENTITY), RigidBody.ACTIVE_TAG, 0);
	}
}