package ecumene.cross.common;

import javax.vecmath.Matrix4f;

public interface ICamera {
	public Matrix4f getProjection();
	public Matrix4f getView();
}
