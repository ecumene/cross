package ecumene.cross.common;

import javax.vecmath.Matrix4f;
import javax.vecmath.Vector2f;
import javax.vecmath.Vector4f;

import org.lwjgl.opengl.GL11;

import ecumene.geom.builder.Face;
import ecumene.geom.builder.MeshBuilder;
import ecumene.geom.builder.Vertex;
import ecumene.geom.ogl.OGLGeomBuffer;
import ecumene.opengl.GLPrimitive;
import ecumene.opengl.OpenGLException;
import ecumene.opengl.texture.ITexture;

// TODO: Better bitmap Font implementation
public class BitmapFont {
	public ITexture parent;
	private OGLGeomBuffer vbo;
	
	public int charSize, gridSize;
	
	public BitmapFont(ITexture parent, int gridSize, int charSize) {
		this.parent = parent;
		vbo = new OGLGeomBuffer();
		this.charSize = charSize; this.gridSize = gridSize;
	}
	
	public void renderString(String string) throws OpenGLException {
		Matrix4f identity = new Matrix4f();
		identity.setIdentity();
		
		renderString(string, identity);
	}
	
    public void renderString(String string, Matrix4f transform) throws OpenGLException {
    	this.renderString(string, charSize, transform);
	}
    
    public void renderString(String string, int charSize) throws OpenGLException {
		Matrix4f identity = new Matrix4f();
		identity.setIdentity();
		
		renderString(string, charSize, identity);
	}
	
	public void renderString(String string, int charSize, Matrix4f transform) throws OpenGLException {
    	vbo.clear();
		GL11.glEnable(GL11.GL_TEXTURE_2D);
		GL11.glEnable(GL11.GL_BLEND);
		
		parent.bind();
		
		GL11.glTexParameterf(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_NEAREST);
		GL11.glTexParameterf(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_NEAREST);
				
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		
		for (int i = 0; i < string.length(); i++) {
			int asciiCode = (int) string.charAt(i);
			final float cellSize = 1.0f / gridSize;
			float cellX = ((int) asciiCode % gridSize) * cellSize;
			float cellY = ((int) asciiCode / gridSize) * cellSize;
			MeshBuilder model = new MeshBuilder(GLPrimitive.TRIANGLES);
			model.add(new Vertex(new Vector4f(i * charSize + charSize, charSize, 0, 1), new Vector2f(cellX + cellSize, cellY + cellSize)));
			model.add(new Vertex(new Vector4f(i * charSize,            charSize, 0, 1), new Vector2f(cellX, cellY + cellSize)));
			model.add(new Vertex(new Vector4f(i * charSize, 		          0, 0, 1), new Vector2f(cellX, cellY)));
			model.add(new Vertex(new Vector4f(i * charSize + charSize,        0, 0, 1), new Vector2f(cellX + cellSize, cellY)));
			model.addFace(0, new Face(0, 1, 2, 2, 3, 0));
			vbo.add(model.getOGLMesh(), transform);
		}
		
		vbo.upload();
		vbo.render();
	}
    
    public OGLGeomBuffer getVBO(){
    	return vbo;
    }
	
	public ITexture getParent(){
		return parent;
	}
	
	public int getCharacterSize(){
		return charSize;
	}
	
	public int getGridSize(){
		return gridSize;
	}
}
