
uniform sampler2D u_sColor0;
uniform float u_aaSpanMax;
uniform float u_aaReduceMin;
uniform float u_aaReduceMul;
uniform vec2 u_inverseFilterTextureSize;
uniform vec2 u_resolution;
uniform float u_vIntensity;
uniform float u_vOuterRadius;
uniform float u_vInnerRadius;

const vec3 luma = vec3(0.299, 0.587, 0.11);	

void main(void){
	float FXAA_SPAN_MAX   = u_aaSpanMax;
	float FXAA_REDUCE_MIN = u_aaReduceMin;
	float FXAA_REDUCE_MUL = u_aaReduceMin;
		
	float lumaTL = dot(luma, texture2D(u_sColor0, gl_TexCoord[0].xy + (vec2(-1.0, -1.0) * u_inverseFilterTextureSize)).xyz);
	float lumaTR = dot(luma, texture2D(u_sColor0, gl_TexCoord[0].xy + (vec2( 1.0, -1.0) * u_inverseFilterTextureSize)).xyz);
	float lumaBL = dot(luma, texture2D(u_sColor0, gl_TexCoord[0].xy + (vec2(-1.0,  1.0) * u_inverseFilterTextureSize)).xyz);
	float lumaBR = dot(luma, texture2D(u_sColor0, gl_TexCoord[0].xy + (vec2( 1.0,  1.0) * u_inverseFilterTextureSize)).xyz);
	float lumaM  = dot(luma, texture2D(u_sColor0, gl_TexCoord[0].xy).xyz);

	vec2 dir = vec2(-((lumaTL + lumaTR) - (lumaBL + lumaBR)), ((lumaTL + lumaBL) - (lumaTR + lumaBR)));
	
	dir = min(vec2(FXAA_SPAN_MAX, FXAA_SPAN_MAX), 
		  max(vec2(-FXAA_SPAN_MAX, -FXAA_SPAN_MAX), 
		  dir * (1.0/(min(abs(dir.x), abs(dir.y)) + max((lumaTL + lumaTR + lumaBL + lumaBR) * (FXAA_REDUCE_MUL * 0.25), FXAA_REDUCE_MIN))))) * u_inverseFilterTextureSize;

	vec3 result1 = (1.0/2.0) * (
		texture2D(u_sColor0, gl_TexCoord[0].xy + (dir * vec2(1.0/3.0 - 0.5))).xyz +
		texture2D(u_sColor0, gl_TexCoord[0].xy + (dir * vec2(2.0/3.0 - 0.5))).xyz);

	vec3 result2 = result1 * (1.0/2.0) + (1.0/4.0) * (
		texture2D(u_sColor0, gl_TexCoord[0].xy + (dir * vec2(0.0/3.0 - 0.5))).xyz +
		texture2D(u_sColor0, gl_TexCoord[0].xy + (dir * vec2(3.0/3.0 - 0.5))).xyz);

	float lumaMin = min(lumaM, min(min(lumaTL, lumaTR), min(lumaBL, lumaBR)));
	float lumaMax = max(lumaM, max(max(lumaTL, lumaTR), max(lumaBL, lumaBR)));
	float lumaResult2 = dot(luma, result2);
	
	if(lumaResult2 < lumaMin || lumaResult2 > lumaMax) gl_FragColor = vec4(result1, 1.0);
	else                                               gl_FragColor = vec4(result2, 1.0);
	
	float vignette = smoothstep(u_vOuterRadius, u_vInnerRadius, length(gl_FragCoord.xy / u_resolution - .5));
	gl_FragColor = mix(gl_FragColor, gl_FragColor * vignette, u_vIntensity);	
}