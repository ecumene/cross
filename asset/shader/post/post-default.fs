uniform sampler2D u_sColor0;
uniform vec2 u_resolution;

void main(void)
{
	gl_FragColor = texture2D(u_sColor0, gl_TexCoord[0].xy);
}