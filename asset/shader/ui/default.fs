#include "./asset/shader/world/global.fsh"

uniform bool u_sampleTexture;
uniform vec4 u_cMod;

vec4 calcPixel()
{
    vec4 p_base;
    if(u_sampleTexture) p_base = texture2D(u_material.map_Kd, v_texcoord);
    else                p_base = vec4(1);
    return u_cMod * p_base;
}