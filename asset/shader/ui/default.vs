#include "./asset/shader/world/global.vsh"

void main(void)
{
	v_position = vec3(gl_Vertex);
	v_texcoord = gl_MultiTexCoord0.xy;
	
	gl_Position = u_tProjection * u_tView * u_tObject * vec4(v_position, gl_Vertex.w);
}