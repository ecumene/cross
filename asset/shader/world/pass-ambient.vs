#include "./asset/shader/world/global.vsh"

void main(void)
{
	v_position = vec3(u_tView * u_tObject * gl_Vertex);
	v_texcoord = gl_MultiTexCoord0.xy;
	v_normal   = gl_Normal;
	
	gl_Position = u_tProjection * vec4(v_position, gl_Vertex.w);
}