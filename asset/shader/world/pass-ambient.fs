#include "./asset/shader/world/global.fsh"

uniform vec3 u_lAmbient;

vec4 calcPixel()
{
	return vec4(u_lAmbient, 1.0) * texture2D(u_material.map_Kd, v_texcoord) + u_material.Ka;
}