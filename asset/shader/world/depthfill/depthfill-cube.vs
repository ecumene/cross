#include "./asset/shader/world/global.vsh"

uniform mat4 u_tLight;
uniform vec3 u_lPosition;

varying vec3 v_lPosition_ws;
varying vec3 v_vPosition_ws;

void main(void)
{
	v_lPosition_ws = u_lPosition;
	v_vPosition_ws = vec3(u_tObject * gl_Vertex);
	gl_Position = u_tLight * vec4(v_vPosition_ws, gl_Vertex.w);
}