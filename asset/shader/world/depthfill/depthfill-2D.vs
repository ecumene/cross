#include "./asset/shader/world/global.vsh"

uniform mat4 u_tLight;

void main(void)
{
	gl_Position = u_tLight * u_tObject * gl_Vertex;
}