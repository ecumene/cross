#version 120
varying vec3 v_position;
varying vec2 v_texcoord;
varying vec3 v_normal;
varying mat3 n_TBN;

uniform sampler2D u_sampler0;
uniform sampler2D u_sampler1;
uniform sampler2D u_sampler2;

struct Material
{
	float specularPower;
	
	vec4 Ka;
	vec4 Kd;
	vec4 Ks;
	
	sampler2D map_Kd;
	sampler2D map_Kn;
	sampler2D map_Ks;
};

uniform Material u_material;

vec4 calcPixel();

void main(void){
	gl_FragColor = calcPixel();
}