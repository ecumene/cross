varying vec3 v_position;
varying vec2 v_texcoord;
varying vec3 v_normal;
varying mat3 n_TBN;

uniform mat4 u_tProjection;
uniform mat4 u_tView;
uniform mat4 u_tObject;

uniform mat3 u_nView;
uniform mat3 u_nObject;