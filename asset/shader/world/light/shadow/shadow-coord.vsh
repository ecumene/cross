varying vec4 v_shadowCoord;
uniform mat4 u_tDepthBias;

void shadowCoord(){
	v_shadowCoord = u_tDepthBias * u_tObject * gl_Vertex;
}