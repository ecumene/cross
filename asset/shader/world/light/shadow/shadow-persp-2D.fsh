uniform sampler2D u_sDepthMap;
varying vec4 v_shadowCoord;

float shadowVisibility(float cosTheta){
	//float bias = 0.005 * tan(acos(cosTheta));
	//bias = clamp(bias, 0, 0.01);
	
	float bias = 0.005;
	
	if(texture2D(u_sDepthMap, v_shadowCoord.xy/v_shadowCoord.w).z < (v_shadowCoord.z - bias) / v_shadowCoord.w) return 0.0;
	else                                                                                                        return 1.0;
}