#include "./asset/shader/world/global.vsh"
#include "./asset/shader/world/light/shadow/shadow-coord.vsh"

uniform vec3 u_lPosition;
uniform vec3 u_lDirection;
varying vec3 l_sPosition;
varying vec3 l_sDirection;

void main(void)
{
	vec3 N = normalize(u_nView * u_nObject * gl_Normal);
	vec3 T = normalize(u_nView * u_nObject * vec3(gl_Color));
	vec3 B = normalize(cross(T, N));
	
	n_TBN = mat3(T, B, N);
	
	l_sPosition  =           vec3(u_tView * vec4(u_lPosition,  1));
	l_sDirection = normalize(vec3(u_tView * vec4(u_lDirection, 0)));
	v_position = vec3(u_tView * u_tObject * gl_Vertex);
	v_texcoord = gl_MultiTexCoord0.xy;
	v_normal   = gl_Normal;
	
	gl_Position = u_tProjection * vec4(v_position, gl_Vertex.w);
	shadowCoord();
}