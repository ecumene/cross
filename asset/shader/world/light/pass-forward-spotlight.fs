#include "./asset/shader/world/light/pass-forward-light.fsh"
#include "./asset/shader/world/light/shadow/shadow-persp-2D.fsh"

uniform SpotLight u_light;

varying vec3 l_sPosition;
varying vec3 l_sDirection;

vec4 calcLightModule(vec3 p_normal, vec3 p_position)
{
	vec3  l_direction = l_sPosition - p_position;
	float dist = length(l_direction);
	l_direction = normalize(l_direction);
	float l_sFactor = dot(l_direction, l_sDirection);
	float cosTheta  = dot(l_direction, p_normal);
	vec4  r_color = vec4(0, 0, 0, 0);
	
	if(l_sFactor > u_light.cutoff){
		r_color = vec4(lightModule(u_light.pointLight.base, u_material, normalize(l_direction), p_normal, p_position, cosTheta) / 
	                                   (u_light.pointLight.atten.constant + u_light.pointLight.atten.linear * dist
	                                    + u_light.pointLight.atten.exponent * dist * dist + 0.0001)) * (1.0 - (1.0 - l_sFactor) / (1.0 - u_light.cutoff));
		if(u_bShadowMap) r_color *= vec4(vec3(shadowVisibility(cosTheta)), 1.0); 
	}
	
	return r_color;
}