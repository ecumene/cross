#include "./asset/shader/world/global.fsh"

uniform bool u_bShadowMap;

struct Light
{
	vec4 color;
};

struct Attenuation
{
	float constant;
	float linear;
	float exponent;
};

struct PointLight
{
	Light       base;
	Attenuation atten;
	float       range;
};

struct SpotLight
{
	PointLight pointLight;
	float      cutoff;
};

vec4 lightModule(Light p_base, Material p_material, vec3 p_direction, vec3 p_normal, vec3 p_position, float cosTheta)
{
	float diffuse = cosTheta;
	vec4 diff = vec4(0, 0, 0, 0);
	vec4 spec = vec4(0, 0, 0, 0);
	
	if((diffuse > 0.0))
	{
		diff = vec4(p_material.Kd.xyz, 1) * vec4(p_base.color.xyz, 1) * vec4(p_base.color.w) *vec4(diffuse);
		vec3 eyeDir = normalize(-p_position);
		vec3 HDir   = normalize(eyeDir + p_direction);
		
		float specular = pow(dot(HDir, p_normal), p_material.specularPower);
		
		if(specular > 0.0)
		{
			vec4 specMap = texture2D(p_material.map_Ks, v_texcoord);
			spec = vec4(p_base.color.xyz, 1.0) * p_base.color.w * vec4(p_material.Ks.xyz, 1) * vec4(p_material.Ks.w) * vec4(specMap.xyz, 1) * vec4(specMap.w) * vec4(specular);
		}
	}
	
	return diff + spec;
}

vec4 calcLightModule(vec3 p_normal, vec3 p_position);

vec4 calcPixel()
{	
	vec4 p_normal = texture2D(u_material.map_Kn, v_texcoord);
	if(p_normal.a == 0.0) return vec4(0.0, 0.0, 0.0, 0.0);
	vec3 p_tNormal = normalize(n_TBN * vec3(p_normal.rgb * 2.0 - 1.0));
	return calcLightModule(p_tNormal, v_position);
}